import numpy as np
import cv2

img = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')

"""
Source : Stackoverflow for the text below
Of course, the implementation is my own work

The gradient of a function of two variables x, y is a vector of the partial derivatives in the x and y direction. So if your function is f(x,y), the gradient is the vector (f_x, f_y). An image is a discrete function of (x,y), so you can also talk about the gradient of an image.

The gradient of the image has two components: the x-derivative and the y-derivative. So, you can think of it as vectors (f_x, f_y) defined at each pixel. These vectors have a direction atan(f_y / fx) and a magnitude sqrt(f_x^2 + f_y^2). So, you can represent the gradient of an image either an x-derivative image and a y-derivative image, or as direction image and a magnitude image.
"""
def img_pixel(img, class_to_pixels):
    height, width = img.shape
    img_bis = np.zeros((height, width, 3))
    for i in range(height):
        for j in range(width):
            index = int(img[i,j])
            img_bis[i,j] = class_to_pixels[index]
    return img_bis.astype(dtype =np.uint8)

def magnitude_gradient(img_bis, class_to_pixels):
    img = img_pixel(img_bis, class_to_pixels)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)#Convert to grey
    kernely = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
    kernelx = np.array([[1,0,-1],[1,0,-1],[1,0,-1]])
    f_x = cv2.filter2D(img,cv2.CV_8U,kernelx)
    f_y = cv2.filter2D(img,cv2.CV_8U,kernely)

    magnitude = np.sqrt(f_x ** 2 + f_y ** 2)

    #cv2.imshow('Gradients_X',f_x)
    #cv2.imshow('Gradients_Y',f_y)
    #print(magnitude)
    return magnitude


#Graph Modelisation
###ALL THIS PART IS BASED ON THE FOLLOWING PAPER
# http://www.cs.cornell.edu/rdz/Papers/BVZ-iccv99.pdf

#L = matrix 3d of all the color porbabilities of each pixel [i,j]

def alpha_beta_graph(alpha, beta, f, L, class_to_pixels): #alpha and beta are color classes. f is given labeling, that is a class of color for each pixel, a 2D matrix
    height, width = f.shape
    magnitude = magnitude_gradient(f, class_to_pixels)
    graph = np.zeros((height, width))
    vertices = np.zeros((height, width))
    weight = np.zeros((height*width + 2, width*height + 2)) #2D matrix wher [i,j] contains the weight of the link from i to j
    for i in range(height):
        for j in range(width):
            if f[i,j] == alpha :
                vertices[i,j] = 1
                graph[i,j] = 1 #when 1, the pixel [i,j] is in the graph we want to create
            if f[i,j] == beta :
                vertices[i,j] = 1
                graph[i,j] = 1
    for i in range(height-1):
        for j in range(width-1):
            if f[i,j] == alpha :
                weight[0, i*width + j] = smooth_cost(alpha, i, j, vertices, f, magnitude)
                weight[i*width + j, 0] = smooth_cost(alpha, i, j, vertices, f, magnitude)
            if f[i,j] == beta :
                weight[0, i*width + j] = smooth_cost(beta, i, j, vertices, f, magnitude)
                weight[i*width + j, 1] = smooth_cost(beta, i, j, vertices, f, magnitude)
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            if vertices[i,j] == 1:
                if vertices[i-1,j-1] == 1:
                    weight[(i-1)*width + j-1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i-1)*width + j-1 +2] = beta - alpha
                elif vertices[i-1,j] == 1:
                    weight[(i-1)*width + j +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i-1)*width + j +2] = beta - alpha
                elif vertices[i-1,j+1] == 1:
                    weight[(i-1)*width + j+1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i-1)*width + j+1 +2] = beta - alpha
                elif vertices[i,j-1] == 1:
                    weight[i*width + j-1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, i*width + j-1 +2] = beta - alpha
                elif vertices[i,j+1] == 1:
                    weight[i*width + j+1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, i*width + j+1 +2] = beta - alpha
                elif vertices[i+1,j-1] == 1:
                    weight[(i+1)*width + j-1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i+1)*width + j-1 +2] = beta - alpha
                elif vertices[i+1,j] == 1:
                    weight[(i+1)*width + j +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i+1)*width + j +2] = beta - alpha
                elif vertices[i+1,j+1] == 1:
                    weight[(i+1)*width + j+1 +2, i*width + j +2] = beta - alpha
                    weight[i*width + j +2, (i+1)*width + j+1 +2] = beta - alpha
                else :
                    break
                #vertices[i,j] == 0
    return graph, weight

def alpha_graph(alpha, f, L):
    height, width = f.shape
    magnitude = magnitude_gradient(f)
    graph = np.zeros((height, width))
    vertices = np.zeros((height, width))
    
    aux_count = auxiliaries(f)
    
    weight = np.zeros((height*width + 2 + aux_count, width*height + 2 + aux_count))
    #vertices are alpha, "non-alpha", image pixels and auxiliaries
    
    for i in range(height):
        for j in range(width):
            if f[i,j] == alpha :
                vertices[i,j] = 1
                graph[i,j] = 1 

    for i in range(height-1):
        for j in range(width-1):
            if f[i,j] == alpha :
                weight[1, i*width + j] = float('inf') #pseudo-infinite value
                weight[i*width + j, 1] = float('inf')
            else:
                dp_fp = (magnitude[i][j] - f[i][j]) **2
                weight[1, i*width + j] = dp_fp
                weight[i*width + j, 1] = dp_fp
    
    count = 0
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            value = f[i,j]
            diff_alpha = abs(alpha - value)
            
            if f[i-1,j-1] == value:
                weight[(i-1)*width + j-1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i-1)*width + j-1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i-1)*width + (j-1) +2, width+count] = abs(f[i-1,j-1]-alpha)
                weight[height + count, (i-1)*width + (j-1) +2] = abs(f[i-1,j-1]-alpha)
                
                weight[1, width+count] = abs(value-f[i-1,j-1])
                weight[height+count,1] = abs(value-f[i-1,j-1])
                
                count +=1
                
            if f[i-1,j] == value:
                weight[(i-1)*width + j +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i-1)*width + j +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i-1)*width + (j) +2, width+count] = abs(f[i-1,j]-alpha)
                weight[height + count, (i-1)*width + (j) +2] = abs(f[i-1,j]-alpha)
                
                weight[1, width+count] = abs(value-f[i-1,j])
                weight[height+count,1] = abs(value-f[i-1,j])
                
                count +=1
                
            if f[i-1,j+1] == value:
                weight[(i-1)*width + j+1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i-1)*width + j+1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i-1)*width + (j+1) +2, width+count] = abs(f[i-1,j+1]-alpha)
                weight[height + count, (i-1)*width + (j+1) +2] = abs(f[i-1,j+1]-alpha)
                
                weight[1, width+count] = abs(value-f[i-1,j+1])
                weight[height+count,1] = abs(value-f[i-1,j+1])
                
                count +=1
                
            if f[i,j-1] == value:
                weight[i*width + j-1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, i*width + j-1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i)*width + (j-1) +2, width+count] = abs(f[i,j-1]-alpha)
                weight[height + count, (i)*width + (j-1) +2] = abs(f[i,j-1]-alpha)
                
                weight[1, width+count] = abs(value-f[i,j-1])
                weight[height+count,1] = abs(value-f[i,j-1])
            
                count +=1
            
            if f[i,j+1] == value:
                weight[i*width + j+1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, i*width + j+1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i)*width + (j+1) +2, width+count] = abs(f[i,j+1]-alpha)
                weight[height + count, (i)*width + (j+1) +2] = abs(f[i,j+1]-alpha)
                
                weight[1, width+count] = abs(value-f[i,j+1])
                weight[height+count,1] = abs(value-f[i,j+1])
                
                count +=1
                
            if f[i+1,j-1] == value:
                weight[(i+1)*width + j-1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i+1)*width + j-1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i+1)*width + (j-1) +2, width+count] = abs(f[i+1,j-1]-alpha)
                weight[height + count, (i+1)*width + (j-1) +2] = abs(f[i+1,j-1]-alpha)
                
                weight[1, width+count] = abs(value-f[i+1,j-1])
                weight[height+count,1] = abs(value-f[i+1,j-1])
                
                count +=1
                
            if f[i+1,j] == value:
                weight[(i+1)*width + j +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i+1)*width + j +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i+1)*width + (j) +2, width+count] = abs(f[i+1,j]-alpha)
                weight[height + count, (i+1)*width + (j) +2] = abs(f[i+1,j]-alpha)
                
                weight[1, width+count] = abs(value-f[i+1,j])
                weight[height+count,1] = abs(value-f[i+1,j])
                
                count +=1
                
            if f[i+1,j+1] == value:
                weight[(i+1)*width + j+1 +2, i*width + j +2] = diff_alpha
                weight[i*width + j +2, (i+1)*width + j+1 +2] = diff_alpha
            else:
                weight[i*width + j +2, width+count] = abs(value-alpha)
                weight[height + count, i*width + j +2] = abs(value-alpha)
                
                weight[(i+1)*width + (j+1) +2, width+count] = abs(f[i+1,j+1]-alpha)
                weight[height + count, (i+1)*width + (j+1) +2] = abs(f[i+1,j+1]-alpha)
                
                weight[1, width+count] = abs(value-f[i+1,j+1])
                weight[height+count,1] = abs(value-f[i+1,j+1])
    
                count +=1
    
    return graph, weight

def auxiliaries(f):
    height, width = f.shape
    
    count = 0
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            value = f[i,j]
            val_neigh = f[i-1:i+1, j-1:j+1].flatten() #8 neighbors
            for elem in val_neigh:
                if (elem != value):
                    count +=1
        
    return count


def neighbors(graph): # each pixel is a neighbor of alpha and beta and of its 8 neighbors in the picture which belong to the graph
    height, width = graph.shape
    neighbors = -1 * np.ones((height * width, 8, 3)) #the 3D array of neighbors contain for each pixel the coordinates of its neighbors
    neighbors_source = []
    if graph[0, 0] == 1:                                 #we treat differently the 4 corners and the 4 margins of the picture
        neighbors_source.append([2,0,0])
        if graph[0, 1] == 1:
            neighbors[0][0] =  [3, 0, 1]
        if graph[1, 0] == 1:
            neighbors[0][1] =  [width  +2, 1, 0]
        if graph[1, 0] == 1:
            neighbors[0][2] =  [width +1 +2, 1, 1]
    if graph[height - 1, 0] == 1:
        neighbors_source.append([(height - 1)*width +2, height - 1,0])
        if graph[height - 1, 1] == 1:
            neighbors[height - 1][0] =  [(height-1) * width +1 +2, height - 1, 1]
        if graph[height - 2, 0] == 1:
            neighbors[height - 1][1] =  [(height-2) * width  +2, height - 2, 0]
        if graph[height - 2, 0] == 1:
            neighbors[height - 1][2] =  [(height-2) * width +1 +2, height - 2, 1]
    if graph[0, width-1] == 1:
        neighbors_source.append([width-1 + 2,0,width-1])
        if graph[0, width-2] == 1:
            neighbors[width - 1][0] =  [width-2 +2, 0, width-2]
        if graph[1, width-1] == 1:
            neighbors[width - 1][1] =  [height+ width-1  +2, 1, width-1]
        if graph[1, width-2] == 1:
            neighbors[width - 1][2] =  [height+ width-2 +2, 1, width-2]
    if graph[height-1, width-1] == 1:
        neighbors_source.append([height*width -1 +2, height-1, width-1])              #################### doubt size
        if graph[height-1, width-2] == 1:
            neighbors[height * width -1][0] =  [height * width + width-1 +2, height-1, width-2]
        if graph[height-2, width-1] == 1:
            neighbors[height * width -1][1] =  [(height-1) * width + width  +2, height-2, width-1]
        if graph[height-2, width-2] == 1:
            neighbors[height * width -1][2] =  [(height-1) * width + width-1 +2, height-2, width-2]

    for i in range(1, height - 1):
        if graph[i, 0] == 1:
            neighbors_source.append([i*width +2, i, 0])
            if graph[i-1, 0] == 1:
                neighbors[i*width][0] =  [(i-1)*width +2, i-1, 0] ####################"doubt
            if graph[i+1, 0] == 1:
                neighbors[i*width][1] =  [(i+1)*width +2, i+1, 0]
            if graph[i-1, 1] == 1:
                neighbors[i*width][2] =  [(i-1)*width +1 +2, i-1, 1]
            if graph[i, 1] == 1:
                neighbors[i*width][3] =  [i*width + 1 +2, i, 1]
            if graph[i+1, 1] == 1:
                neighbors[i*width][4] =  [(i+1)*width +1 +2, i+1, 1]
    for i in range(1, height - 1):
        if graph[i, width - 1] == 1:
            neighbors_source.append([i*width + width - 1 +2, i, height - 1])
            if graph[i-1, width - 1] == 1:
                neighbors[i*width + width - 1][0] =  [(i-1)*width + width-1 +2, i-1, width-1] ####################"doubt
            if graph[i+1, width - 1] == 1:
                neighbors[i*width + width - 1][1] =  [(i+1)*width + width-1 +2, i+1, width-1]
            if graph[i-1, width - 2] == 1:
                neighbors[i*width + width - 1][2] =  [(i-1)*width + width-2 +2, i-1, width-2]
            if graph[i, width - 2] == 1:
                neighbors[i*width + width - 1][3] =  [i*width + width-2 +2, i, width-2]
            if graph[i+1, width - 2] == 1:
                neighbors[i*width + width - 1][4] =  [(i+1)*width + width-2 +2, i+1, width-2]
    for j in range(1, width - 1):
        if graph[0, j] == 1:
            neighbors_source.append([j +2, 0, j])
            if graph[0, j-1] == 1:
                neighbors[j][0] =  [j-1 +2, 0, j-1] ####################"doubt
            if graph[0, j+1] == 1:
                neighbors[ j][1] =  [j+1 +2, 0, j+1]
            if graph[1, j-1] == 1:
                neighbors[ j][2] =  [j-1 +2, 1, j-1]
            if graph[1, j] == 1:
                neighbors[ j][3] =  [j +2, 1, j]
            if graph[1, j+1] == 1:
                neighbors[ j][4] =  [j+1 +2, 1, j+1]
    for j in range(1, width - 1):
        if graph[height - 1, j] == 1:
            neighbors_source.append([(height - 1)*width +j +2, 0, j])
            if graph[height - 1, j-1] == 1:
                neighbors[(height-1)*width + j][0] =  [(height - 1)*width +j-1 +2, height - 1, j-1] ####################"doubt
            if graph[height - 1, j+1] == 1:
                neighbors[(height-1)*width + j][1] =  [(height - 1)*width +j+1 +2, height - 1, j+1]
            if graph[height - 2, j-1] == 1:
                neighbors[(height-1)*width + j][2] =  [(height - 2)*width +j-1 +2, height - 2, j-1]
            if graph[height - 2, j] == 1:
                neighbors[(height-1)*width + j][3] =  [(height - 2)*width +j +2, height - 2, j]
            if graph[height - 2, j+1] == 1:
                neighbors[(height-1)*width + j][4] =  [(height - 2)*width +j+1 +2, height - 2, j+1]

    for i in range(1, height - 1):
        for j in range(1, width - 1):
            if graph[i, j] == 1:
                neighbors_source.append([i*width + j +2, i, j])
                if graph[i-1, j-1] == 1:
                    neighbors[i*width + j][0] =  [(i-1)*width + j-1 +2, i-1, j-1] ####################"doubt +2 ?
                if graph[i-1, j] == 1:
                    neighbors[i*width + j][1] =  [(i-1)*width + j +2, i-1, j]
                if graph[i-1, j+1] == 1:
                    neighbors[i*width + j][2] =  [(i-1)*width + j+1 +2, i-1, j+1]
                if graph[i, j-1] == 1:
                    neighbors[i*width + j][3] =  [i*width + j-1 +2, i, j-1]
                if graph[i, j+1] == 1:
                    neighbors[i*width + j][4] =  [i*width + j+1 +2, i, j+1]
                if graph[i+1, j-1] == 1:
                    neighbors[i*width + j][5] =  [(i+1)*width + j-1 +2, i+1, j-1]
                if graph[i+1, j] == 1:
                    neighbors[i*width + j][6] =  [(i+1)*width + j +2, i+1, j]
                if graph[i+1, j+1] == 1:
                    neighbors[i*width + j][7] =  [(i+1)*width + j+1 +2, i+1, j+1]
    return neighbors, neighbors_source

###NEIGHBORS CASE 1



def smooth_cost(alpha, i, j, vertices, label, magnitude):
    res = 0
    #magnitude = magnitude_gradient(label)
    if vertices[i-1,j-1] == 0:
        res += magnitude[i-1,j-1] * (label[i-1,j-1] - alpha)
    elif vertices[i-1,j] == 0:
        res += magnitude[i-1,j] * (label[i-1,j] - alpha)
    elif vertices[i-1,j+1] == 0:
        res += magnitude[i-1,j+1] * (label[i-1,j+1] - alpha)
    elif vertices[i,j-1] == 0:
        res += magnitude[i,j-1] * (label[i,j-1] - alpha)
    elif vertices[i,j+1] == 0:
        res += magnitude[i,j+1] * (label[i,j+1] - alpha)
    elif vertices[i+1,j-1] == 0:
        res += magnitude[i+1,j-1] * (label[i+1,j-1] - alpha)
    elif vertices[i+1,j] == 0:
        res += magnitude[i+1,j] * (label[i+1,j] - alpha)
    elif vertices[i+1,j+1] == 0:
        res += magnitude[i+1,j+1] * (label[i+1,j+1] - alpha)
    return res


def edmonds_karp(graph, weight):  #Edmonds-Karp Algorithm
    source, destination = 0, 1
    height, width = graph.shape
    neighbours, neighbors_source = neighbors(graph)
    flow = 0 # maximal flow
    F = np.zeros((height*width + 2, width*height + 2))#maximal flow
    capacity, Parents = breadth_first_search(weight, neighbours, neighbors_source, F)
    while capacity != 0:
        capacity, Parents = breadth_first_search(weight, neighbours, neighbors_source, F)
        flow += capacity
        v = 1
        while v != 0 :
            u = Parents[v]
            F[u,v] = F[u,v] + capacity
            F[v,u] = F[v,u] - capacity
            v = u

    return flow, F

def breadth_first_search(weight, neighbors, neighbors_source, F):
    height, width = weight.shape
    Parents = -1  * np.ones((height, width))
    capacity = np.zeros((height * width))
    Parents[0] = -2
    capacity[0] = 10000  #infinity
    q = [0]
    while len(q) > 0:
        u = q.pop(0)
        if u == 0 or u == 1 :
            for v in neighbors_source:
                #print(v, neighbors_source)
                if weight[u,int(v[0])]- F[u,int(v[0])] > 0 and Parents[int(v[1]), int(v[2])] == -1 :
                    Parents[int(v[1]),int(v[2])] = u
                    capacity[int(v[0])] = min(capacity[u], weight[u,int(v[0])]- F[u,int(v[0])])
                    if v[0] != 1:
                        q.append(int(v[0]))
                    else:
                        return capacity[1], Parents
        else :
            for v in neighbors[u]:
                if v[0] != -1:
                    if weight[u,int(v[0])]- F[u,int(v[0])] > 0 and Parents[int(v[1]), int(v[2])] == -1 :
                        Parents[int(v[1]),int(v[2])] = u
                        capacity[int(v[0])] = min(capacity[u], weight[u,int(v[0])]- F[u,int(v[0])])
                        if v[0] != 1:
                            q.append(int(v[0]))
                        else:
                            return capacity[1], Parents
    return 0, Parents


def breadth_first_search_from_source(F, weight):
    Res = weight - F
    f = [0]
    marked = [0]
    while len(f) > 0:
        s = f.pop(len(f)-1)
        for i in range(len(Res)):
            if Res[s, i] != 0 and marked.count(i) == 0 :
                f.append(i)
                marked.append(i)
    return marked



def minimum_graph_cut(graph, weight):
    flow, F = edmonds_karp(graph, weight)
    marked = breadth_first_search_from_source(F, weight)
    height, width = graph.shape
    S = np.zeros((height+2, width+2))
    T = np.zeros((height+2, width+2))
    S[0,0] = 1
    T[1,1] = 1
    for i in range(height):
        for j in range(width):
            if graph[i,j] == 1:
                if marked.count(i*width + j) == 0 :
                    T[i+2,j+2] = 1
                else :
                    S[i+2,j+2] = 1
    return S, T

def minimal_labeling(graph, S, T, label, alpha, beta):
    label_bis = label
    height, width = graph.shape
    for i in range(height):
        for j in range(width):
            if graph[i,j] == 1:
                if S[0,0] == 1 and T[i+2, j+2] == 1:
                    label_bis[i,j] = alpha
                elif T[0,0] == 1 and S[i+2, j+2] == 1:
                    label_bis[i,j] = alpha
                elif S[1,1] == 1 and T[i+2, j+2] == 1:
                    label_bis[i,j] = beta
                elif T[0,0] == 1 and S[i+2, j+2] == 1:
                    label_bis[i,j] = beta
                #else: keep the previous label
    return label_bis

def miniswap_labeling(graph, S, T, label, alpha):
    label_bis = label
    height, width = graph.shape
    for i in range(height):
        for j in range(width):
            if graph[i,j] == 1:
                if S[0,0] == 1 and T[i+2, j+2] == 1:
                    label_bis[i,j] = alpha
                elif T[0,0] == 1 and S[i+2, j+2] == 1:
                    label_bis[i,j] = alpha
                #else: keep the previous label
    return label_bis



def idd(a, b):
    if a == b:
        return 1
    else :
        return 0

def label_costs(i,j, label, L, K):
    label_cost = 0
    for k in range(len(K)):
        label_cost += L[i, j, k] * idd(k, label[i,j])
    return label_cost



def energy_function(label, L, K, class_to_pixels):
    magnitude = magnitude_gradient(label, class_to_pixels)
    pixel_energy = 0
    height, width, u = L.shape
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            pixel_energy += magnitude[i-1,j-1] * (label[i-1,j-1] - label[i,j])
            pixel_energy += magnitude[i-1,j] * (label[i-1,j] - label[i,j])
            pixel_energy += magnitude[i-1,j+1] * (label[i-1,j+1] - label[i,j])
            pixel_energy += magnitude[i,j-1] * (label[i,j-1] - label[i,j])
            pixel_energy += magnitude[i,j+1] * (label[i,j+1] - label[i,j])
            pixel_energy += magnitude[i+1,j-1] * (label[i+1,j-1] - label[i,j])
            pixel_energy += magnitude[i+1,j] * (label[i+1,j] - label[i,j])
            pixel_energy += magnitude[i+1,j+1] * (label[i+1,j+1] - label[i,j])

            pixel_energy -= label_costs(i,j, label, L, K)
    return pixel_energy



def graph_cut(K, L, class_to_pixels):
    #start with an arbitrary labeling label
    #2 set success = 0
    # For each pair of labels {α, β} ⊂ K color classes ensemble
    # 3   Find ˆf = arg min E(f′) among f ′ withinone α-β swap of f
    # 4    If E( ˆf ) < E(f ), set f := ˆf and success := 1
    #If success = 1 goto 2
    height, width, u = L.shape
    label = np.random.randint(len(K), size=(height, width))
    #print(label)
    success = 1
    while success == 1 :
        success = 0
        for i in range(0, len(K)-1):
            for j in range(i+1, len(K)):
                alpha = K[i]
                beta = K[j]
                graph, weight = alpha_beta_graph(alpha, beta, label, L, class_to_pixels)
                S, T = minimum_graph_cut(graph, weight)
                label_bis = minimal_labeling(graph, S, T, label, alpha, beta)
                if energy_function(label_bis, L, K, class_to_pixels) < energy_function(label, L, K, class_to_pixels):
                    label = label_bis
                    success = 0

    return label

def swap_cut(K,L):
    height, width, u = L.shape
    label = np.random.randint(len(K), size=(height, width))
    success = 1
    while success == 1 :
        success = 0
        for i in range(0, len(K)-1):
                alpha = K[i]
                graph, weight = alpha_graph(alpha, label, L)
                S, T = minimum_graph_cut(graph, weight)
                label_bis = miniswap_labeling(graph, S, T, label, alpha)
                if energy_function(label_bis, L, K) < energy_function(label, L, K):
                    label = label_bis
                    success = 1

    return label


def test():
    image = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')
    #cv2.imshow('',image)
    image = image[:10, :10]
    #print(image)
    class_to_pixels = np.array([[12, 35, 67], [123, 56, 136], [20, 156, 45]]).astype(int)   #### for testing !!!
    #magnitude_gradient(image)
    K = [0,1,2]
    L = image/256
    label = graph_cut(K, L, class_to_pixels)
    print("test finished :", label)







