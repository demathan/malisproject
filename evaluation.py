import numpy as np

def img_accuracy(img_colour, img_predicted):
    height_img, width_img = img_colour.shape
    error = 0

    for i in range(height_img) :
        for j in range(width_img) :
            if img_colour[i][j] != img_predicted[i][j] :
                error += 1

    return error /(height_img * width_img)


def accuracy():
    img_accuracy()


def visual_discrepancy(img_colour, img_predicted, criterion = 0.8, nb_classes = 16):
    '''
    Provides a rough accuracy measure by comparing how "smooth" the predicted image is
    compared to the original one, that is how many perturbations there are in global
    regions composing the image

    Parameters
    ----------
    img_colour : 2D-array
        The original image as a matrix.
    img_predicted : 2D-array
        The predicted image as a matrix.
    criterion : int
        The required smoothness for identifying regions in an image. The default is 0.8
    nb_classes : int, optional
        Number of color classes used. The default is 16.

    Returns
    -------
    int
        A number between 0 (if the smoothnesses are the same) and 1 (if they
        are not similar at all.)

    '''
    height_img, width_img = img_colour.shape

    smoothness, psmoothness = 0, 0

    regions = split(img_colour, criterion, nb_classes)
    pregions = split(img_predicted, criterion, nb_classes)
    
    #Computing smoothnesses
    for reg in regions:
        smoothness += smooth(reg, nb_classes)
    for preg in pregions:
        psmoothness += smooth(preg, nb_classes)
    diff_segmentation = (len(regions) - len(pregions)) / (height_img * width_img -1) #score up to 1 measuring discrepancy between pictures separation into regions
    return abs(psmoothness-smoothness) * diff_segmentation / (height_img * width_img) #up to 1 


def split(list_regions, criterion = 0.8, nb_classes = 16):
    '''
        Recursive function which divides regions into 4 regions of about the same size while
        they do not meet a uniformity criterion.

    Parameters
    ----------
    list_regions : numpy array of matrices
        A list containing the regions that might have to be splitted.
    criterion : float
        The required smoothness expected from a region in order not to be splitted (see smooth function).

    Returns
    -------
    numpy array
        The numpy array containing the splitted regions.

    '''
    result = list_regions
 
    for reg in list_regions :
        height, width = reg.shape
        
        #Worst case safeguard (single pixel regions)
        if ((height == 1) and (width == 1)):
            continue
        
        smoothness = smooth(reg)
        
        #If the region is not smooth enough, we split it, else we continue with the next 
        #one in result
        if(smoothness < criterion):
            vmid, hmid = height//2, width//2
            r1 = reg[0:vmid][0:hmid]
            r2 = reg[0:vmid][hmid:width]
            r3 = reg[vmid:height][0:hmid]
            r4 = reg[vmid:height][hmid:width]
            
            separations = np.array([r4,r3,r2,r1])
            
            #Replacing first region of array with the four sub-regions
            result = result.remove(reg)
            result = result.reverse()
            result = result.extend(separations)
            result = result.reverse()
            
            #Applying split to new regions
            result = split(result)
           
    return result

def smooth(region, nb_classes = 16):
    
    height, width = region.shape
    classes = np.zeros(nb_classes)
    for element in region:
        classes[element] += 1
    majority = np.max(classes)
    return majority / (height * width) #proportion up to 1 representing the "visual homogeneity"
    

            
            
    