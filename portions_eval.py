from scipy.spatial import distance # you can select the euclidean distance
from scipy import stats #
import sys
import numpy as np
from matplotlib import pyplot as plt
import cv2
from sklearn.cluster import KMeans
from scipy.spatial import distance #Scipy is very fast to compute the distance between two vectors
import math
import random
#import no_additional_features.py

main_image=cv2.imread(r'.\palmier.jpg')
main_image_grey = cv2.cvtColor(main_image, cv2.COLOR_BGR2GRAY)

def select_random(img): #selects random pixels that accounts to nearly 5% of the image
    height, width, p = img.shape
    h = math.floor(height / 20)
    w = math.floor(width / 20)
    img_bis = np.zeros((h,w,3))
    for i in range(h):
        for j in range(w):
            i_bis = random.randrange(height)
            j_bis = random.randrange(width)
            img_bis[i,j] = img[i_bis, j_bis]
    print(img_bis.shape)
    return img_bis.astype(dtype =np.uint8)


def select_random_portion(img): #selects random pixels that accounts to nearly 5% of the image
    height, width, p = img.shape
    h = math.floor(height /20)
    w = math.floor(width / 20)
    img_bis = np.zeros((h,w,3))
    i_0=random.randrange(h,height-h)
    j_0=random.randrange(w, width-w)
    img_bis=img[i_0:i_0+h,j_0:j_0+w]
    #print(img_bis.astype(dtype =np.uint8))
   
    return img_bis.astype(dtype =np.uint8)

def preparation():
    #image = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')
    image = cv2.imread(r'.\palmier.jpg')
    #image = image[:40, :30]
    image = select_random(image)

    img_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)#Convertir en gris
    #cv2.imshow('',image)

    height, width = img_grey.shape
    print(height, width)
    pixels_img_grey = np.zeros((height * width))
    color = np.zeros((height, width))
    pixels_img = [image[i,j] for i in range(height) for j in range(width)]

    #Kmeans algo to classify each RGB pixel of a color image into one of the 16 classes
    kmeans = KMeans(16)
    # Compute cluster centers and predict cluster index for each sample
    pixels_img_km = kmeans.fit_predict(pixels_img)
    # RGB coordinates of cluster centers

    pixels_imgNew = kmeans.cluster_centers_

    cluster_centers = kmeans.cluster_centers_
    #Transformation in an appropriate data structure if needed
    for i in range(height):
        for j in range(width):
            #print(pixels_imgNew[i*width + j])
            color[i][j] = pixels_img_km[i*(width-1) + j]
            pixels_img_grey[i*(width-1) + j] = img_grey[i][j]

    return image, img_grey, color, pixels_img_grey, height, width, pixels_imgNew, cluster_centers

"""
###### INTERESTING FUNCTIONS ########
#Discrete Fourier Transformation magnitud
f = np.fft.fft2(img_grey) #Discrete Fourier Transform
fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
magnitude_spectrum = 20*np.log(np.abs(fshift))

#SIFT descriptors
#surf = cv2.xfeatures2d.SURF_create(400) # Create SURF object. Hessian Threshold to 400
#kp, des = surf.detectAndCompute(img_grey, None) # Find keypoints and descriptors directly
sift = cv2.SIFT_create() # Create SIFT object
kp, descriptors = sift.detectAndCompute(img_grey, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly

# localized mean and standard di=eviation of the intensity
mean = np.average(img_grey)
deviation = np.std(img_grey)
#print(mean, deviation)

# grayscale histogram
grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
"""

class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, K):
        '''
        INPUT :
        - K : is a natural number bigger than 0
        '''
        # empty initialization of X and y
        self.X = []
        self.y = []
        # K is the parameter of the algorithm representing the number of neighborhoods
        self.k = K

    """def train(self,X,y):
        '''
        INPUT :
        - X : is a 2D Nx4 numpy array containing the features
        - y : is a 1D Nxk or N numpy array containing the colors
        '''
        self.X=X # copy your training points
        self.y=y"""

    def predict(self,X_new):
        '''
        INPUT :
        - X_new : is a Mx4 numpy array containing the features of new pixels whose label has to be predicted

        OUTPUT :
        - y_hat : is a Mx1 numpy array containing the predicted labels for the X_new points
        '''
        y_hat = []
        for x in X_new:
            distance = []
            voisin = []
            for p in self.X :
                distance.append((x - p)**2)
            for i in range(self.k):
                v = np.argmin(distance)
                distance.pop(v)
                voisin.append(self.y[v])
            y_hat.append((1/len(voisin)*sum(voisin)))

        return y_hat

    def predict_bis(self,X_new, height, width):
        '''
        INPUT :
        - X_new : is a numpy array containing the feature of the new pixel whose label has to be predicted

        OUTPUT :
        - y_hat : is the predicted labels for the X_new points
        '''
        dst = []
        voisin = []
        coordinates = []
        for i in range(height) :
            for j in range(width) :
                try :
                    dst.append(distance.euclidean(self.X[i,j], X_new))
                except ValueError:
                    print(i,j)
                coordinates.append([i,j])
        i=0
        while i<self.k and dst!=[]:
            i+=1
            v = np.argmin(dst)
            dst.pop(v)
            voisin.append(self.y[coordinates[v][0], coordinates[v][1]])

        y_hat = (1/len(voisin)*sum(voisin))

        return y_hat


def prepare_feature(img_grey, height, width):
    delta=5

    x_magnitudeSpectrum = np.zeros((height, width, delta**2))
    #x_grayHist = np.zeros((height, width, 256))
    x_grayHist = np.zeros((height, width))
    x_mean = np.zeros((height, width))
    x_deviation = np.zeros((height, width))
    x_descriptors = np.zeros((height, width))
    radius=delta//2
    for i in range(height) :
        for j in range(width) :
            if (radius<=i<=height-radius-1) and (radius<=j<=width-radius-1) : #change height-radius by height-radius-1 and width-radius by width-radius-1        p = 0
                window=main_image_grey[i-radius: i+radius+1, j-radius: j+radius+1] #change i-radius+1: i+radius by i-radius: i+radius+1
                #print("a", len(window), len(window[0]), i-radius, i+radius+1, j-radius, j+radius+1, i, j)
                p = 0
            else :
                a = i-radius
                b = i+radius+1
                c = j-radius
                d = j+radius+1
                if (i < radius) :
                    a = 0
                    b = delta
                    p = 1
                elif (i > height - 1 - radius) :
                    a = height - 1 -delta
                    b = height - 1
                    p = 2
                if (j < radius):
                    c = 0
                    d = delta
                    p = 3
                elif (j > width - 1 - radius):
                    c = width - 1 -delta
                    d = width - 1
                    p = 4
                window = img_grey[a:b, c:d]

            f = np.fft.fft2(window) #Discrete Fourier Transform
            fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
            magnitude_spectrum = 20*np.log(np.abs(fshift))
            #print(len(magnitude_spectrum), len(magnitude_spectrum[0]))
            try:
                x_magnitudeSpectrum[i,j]=np.array(magnitude_spectrum).ravel()
            except ValueError:
                print(i,j,p,a,b, c, d)

            grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
            #x_grayHist[i,j]=np.array(grayscale_hist).ravel()
            x_grayHist[i,j]=img_grey[i, j]   ###Attention

            mean = np.average(window)
            deviation = np.std(window)
            x_mean[i,j]=mean
            x_deviation[i,j]=deviation

            """sift = cv2.SIFT_create() # Create SIFT object
            kp, descriptors = sift.detectAndCompute(window, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly
            x_descriptors[i,j] =  descriptors""" #Problem of size

    return x_magnitudeSpectrum[:,:,:12], x_grayHist, x_mean, x_deviation

def prepare_feature_pixel(img_grey, i, j, height, width):
    delta=5
    radius=delta//2

    if (radius<=i<=height-radius-1) and (radius<=j<=width-radius-1) : #change height-radius by height-radius-1 and width-radius by width-radius-1
        window=img_grey[i-radius: i+radius+1, j-radius: j+radius+1] #change i-radius+1: i+radius by i-radius: i+radius+1

    else :
        a = i-radius
        b = i+radius+1
        c = j-radius
        d = j+radius+1
        if (i < radius) :
            a = 0
            b = delta
        elif (i > height - 1 - radius) :
            a = height - 1 -delta
            b = height - 1
        if (j < radius):
            c = 0
            d = delta
        elif (j > width - 1 -radius):
            c = width - 1 -delta
            d = width - 1
        window = img_grey[a:b, c:d]

    f = np.fft.fft2(window) #Discrete Fourier Transform
    fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
    magnitude_spectrum = 20*np.log(np.abs(fshift))
    #print("magnitude_spectrum", magnitude_spectrum)
    x_magnitudeSpectrum=np.array(magnitude_spectrum).ravel()
    x_magnitudeSpectrum=x_magnitudeSpectrum[:12]
    x_magnitudeSpectrum[np.isneginf(x_magnitudeSpectrum)]=-10000

    grayscale_hist = cv2.calcHist([window], [0], None, [256], [0, 256])  # window or img_grey
    #x_grayHist=np.array(grayscale_hist).ravel()
    x_grayHist=img_grey[i, j]   ###Attention

    mean = np.average(window)
    deviation = np.std(window)
    x_mean=mean
    x_deviation=deviation

    """sift = cv2.SIFT_create() # Create SIFT object
    kp, descriptors = sift.detectAndCompute(window, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly
    x_descriptors =  descriptors""" #Problem of size

    return x_magnitudeSpectrum, x_grayHist, x_mean, x_deviation

def knn_tests(img_grey, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, color):
    x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation = prepare_feature_pixel(img_grey, i, j, height, width)
    #print(x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation)
    k=10
    knn = KNN(K=k)
    knn.X = img_grey
    knn.y = color
    rep_grey = knn.predict_bis(img_grey[i,j], height, width)

    knn = KNN(K=k)
    knn.X = NewmagnitudeSpectrum
    knn.y = color
    rep_magnitudeSpectrum = knn.predict_bis(x_NewmagnitudeSpectrum, height, width)

    knn = KNN(K=k)
    knn.X = NewgrayHist
    knn.y = color
    rep_grayHist = knn.predict_bis(x_NewgrayHist, height, width)

    knn = KNN(K=k)
    knn.X = Newdeviation
    knn.y = color
    rep_deviation = knn.predict_bis(x_Newdeviation, height, width)

    knn = KNN(K=k)
    knn.X = Newmean
    knn.y = color
    rep_mean = knn.predict_bis(x_Newmean, height, width)

    #print([rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation])
    return np.array([rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation])


def knn_tests_results(img_unknown, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, img_grey, color):  #used for colorization
    x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation = prepare_feature_pixel(img_unknown, i, j, height, width)
    #print(x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation)
    k=10
    knn = KNN(K=k)
    knn.X = img_grey
    knn.y = color
    rep_grey = knn.predict_bis(img_unknown[i,j], height, width)

    knn = KNN(K=k)
    knn.X = NewmagnitudeSpectrum
    knn.y = color
    rep_magnitudeSpectrum = knn.predict_bis(x_NewmagnitudeSpectrum, height, width)

    knn = KNN(K=k)
    knn.X = NewgrayHist
    knn.y = color
    rep_grayHist = knn.predict_bis(x_NewgrayHist, height, width)

    knn = KNN(K=k)
    knn.X = Newdeviation
    knn.y = color
    rep_deviation = knn.predict_bis(x_Newdeviation, height, width)

    knn = KNN(K=k)
    knn.X = Newmean
    knn.y = color
    rep_mean = knn.predict_bis(x_Newmean, height, width)

    #print([rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation])
    return np.array([rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation])

"""
knn = KNN(K=5)
knn.X = pixels_img_grey[0:200]
knn.y = pixels_imgNew[0:200]
rep = knn.predict(pixels_img_grey[0:200])
print(rep, pixels_imgNew[0:200])
"""

### Linear Regression over the answers given by knn for each feature
# We do not know exactly if each feature is really important, so we use a linear regression to attibute to each feature its proper weight

class linear_regression:
    def __init__(self) : # initialize constructor for the object to assign the object its properties
        self.X_train = [] #Contains the features of each pixel
        self.y_train = [] #Contains the color of each pixel
        self.weights = []

    def fit(self, X, y) :
        self.X_train = X
        self.y_train = y
        self.weights = np.linalg.solve(X.T@X,X.T@y)

    def test(self,x_test,y_test) : # Compute the MSE
        self.y_hat=np.sum(x_test*self.weights,axis=1)

        self.MSE= (1/y_test.shape[0])*np.sum(np.square(y_test - self.y_hat))

        return self.y_hat, self.MSE

    def predict(self,x_toPredict) : # predict the color of a pixel
        self.y_hat=np.sum(x_toPredict*self.weights)

        return self.y_hat



def prepare_data_bis(m, n, color):
    input_train =  np.zeros((m, n))
    """TO CHANGE"""
    #input_train =  np.zeros((m, n, 5))
    for i in range(m):
        for j in range(n):
            input_train[i,j] = knn_tests()
    return input_train, color[:m, :n]

def prepare_data(img_grey, height, width, color):
    #print(img_grey)
    m, n = img_grey.shape
    NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
    input_train =  np.zeros((m, n, 5)) #3 rajouté car self.y a trois colonnes

    for i in range(m):
        for j in range(n):
            #print(knn_tests(img_grey, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, color), "ici")
            input_train[i,j] = knn_tests(img_grey, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, color)
    return input_train, color


def prepare_unknown_data_bis(m, n):
    input =  np.zeros((m, n, 5))
    #NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
    for i in range(m):
        for j in range(n):
            input[i,j] = knn_tests()
    return input

def prepare_unknown_data(img, height, width, img_grey, color):
    m, n = img.shape
    NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
    input =  np.zeros((m, n, 5))
    for i in range(m):
        for j in range(n):
            input[i,j] = knn_tests_results(img, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, img_grey, color)
    return input





def colorised_image_ter(img_toColorize, class_to_pixels, model_linear_regression, img_grey, color):
    height_toColorize, width_toColorize = img_toColorize.shape
    color_img = np.zeros((height_toColorize, width_toColorize, 3))
    #input = np.zeros((height_toColorize, width_toColorize, 5))
    input = prepare_unknown_data(img_toColorize, height_toColorize, width_toColorize, img_grey, color)  #fault
    for i in range(height_toColorize) :
        for j in range(width_toColorize) :
            #input = prepare_unknown_data(img_toColorize)
            class_prediction = model_linear_regression.predict(input[i][j])
            class_prediction = math.floor(class_prediction)
            #print(class_prediction)
            pixel = class_to_pixels[class_prediction]
            for l in range(3):
                pixel[l] = math.floor(pixel[l])
            color_img[i][j] = pixel

    return color_img.astype(dtype =np.uint8)

def colorised_image(img_toColorize, class_to_pixels, model_linear_regression, img_grey, color):
    height_toColorize, width_toColorize = img_toColorize.shape
    height, width = img_grey.shape
    color_img = np.zeros((height_toColorize, width_toColorize, 3))
    for i in range(height_toColorize) :
        for j in range(width_toColorize) :
            m, n = img_grey.shape
            NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
            input = knn_tests_results(img_toColorize, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, img_grey, color)
            class_prediction = model_linear_regression.predict(input)
            class_prediction = math.floor(class_prediction)
            #print(class_prediction)
            try:
                pixel = class_to_pixels[class_prediction]
            except IndexError:
                pixel = class_to_pixels[15]
                print("class_prediction" , class_prediction)
            for l in range(3):
                pixel[l] = math.floor(pixel[l])
            color_img[i][j] = pixel

    return color_img.astype(dtype =np.uint8)

def colorised_image_bis(img_toColorize, model_linear_regression, img_grey,color): #returns the colour class
    height_toColorize, width_toColorize = img_toColorize.shape
    color_img = np.zeros((height_toColorize, width_toColorize))
    input = prepare_unknown_data(img_toColorize, height_toColorize, width_toColorize, img_grey, color)

    for i in range(height_toColorize) :
        for j in range(width_toColorize) :
            class_prediction = model_linear_regression.predict(input[i][j])
            class_prediction = math.floor(class_prediction)
            color_img[i][j] = class_prediction

    return color_img.astype(dtype=np.uint8)


def test_prog():

    image, img_grey, color, pixels_img_grey, height, width, pixels_imgNew, class_to_pixels = preparation()
    #cv2.imshow('img',image)
    resize_img = cv2.resize(image  , (320 , 240))
    #cv2.imshow('img' , resize_img)

    input_train, output_train = prepare_data(img_grey, height, width, color)
    print("done")
    input_test, output_test = input_train, output_train
    #input_test, output_test = prepare_data(img_grey, height, width, color)
    #input = [rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation]

    #input = prepare_unknown_data(m, n)

    #There could be some problem of data representation
    #so you need to transform them in an appropriate data structure if needed
    input_train_bis = np.zeros((height * width, 5))
    output_train_bis = np.zeros((height * width))
    input_test_bis = np.zeros((height * width, 5))
    output_test_bis = np.zeros((height * width))

    for i in range(height):
        for j in range(width):

            input_train_bis[i*width + j] = input_train[i][j]
            output_train_bis[i*width + j] = output_train[i][j]
            input_test_bis[i*width + j] = input_test[i][j]
            output_test_bis[i*width + j] = output_test[i][j]

    model_linear_regression = linear_regression()
    model_linear_regression.fit(input_train_bis, output_train_bis)
    model_linear_regression.test(input_test_bis, output_test_bis)
    
    #image = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')
    img_toColorize = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)#Convertir en gris           
    classes_img = colorised_image_bis(img_toColorize, model_linear_regression, img_grey, color) 
    #print(classes_img.shape)
    #classes_img_resize = cv2.resize(classes_img, (320 , 240))
    
    #MSE_add_list, weights, to_show=results(img_grey,resize_img,colorised_img_resize,class_to_pixels,model_linear_regression,color)
    return color.astype(dtype=np.uint8), classes_img

def results(img_grey,resize_img,colorised_img_resize,class_to_pixels,model_linear_regression,color) :
    image = cv2.imread(r'.\palmier.jpg')
    
   # height, width = img_grey.shape
    nb_test=3
    MSE_add_list=[]
    MSE_no_add_list=[]
    to_show=np.zeros((480,3200,3))
    to_show[ :240,:320]=resize_img
    to_show[ 240:,:320]=colorised_img_resize
    
    for k in range(nb_test) : 
        image_k=select_random_portion(image)
        print(image_k.shape)
        resize_img = cv2.resize(image_k  , (320, 240))
        img_toColorize = cv2.cvtColor(image_k, cv2.COLOR_BGR2GRAY)#Convertir en gris
        #cv2.imshow('',image)           
        colorised_img = colorised_image(img_toColorize, class_to_pixels, model_linear_regression, img_grey, color)    
        colorised_img_resize = cv2.resize(colorised_img  , (320 , 240))
        #cv2.imshow('colorized',colorised_img_resize)
        #cv2.imshow('colorized',colorised_img)
        MSE_add=0
        to_show[ :240,320*(k+1):320*(k+2)]=resize_img
        to_show[ 240:,320*(k+1):320*(k+2)]=colorised_img_resize
        height_toColorize,width_toColorize,l=colorised_img_resize.shape
        print("shape:", height_toColorize, width_toColorize)
        for i in range(height_toColorize) :
            for j in range(width_toColorize) :
                MSE_add+=distance.euclidean(colorised_img_resize[i,j],resize_img[i,j])
        MSE_add/=height_toColorize*width_toColorize
        print(MSE_add)
        MSE_add_list.append(MSE_add)
    cv2.imshow('test', to_show.astype(dtype =np.uint8))
    weights=model_linear_regression.weights
    print( MSE_add_list, weights)
    return MSE_add_list, weights, to_show

real_classes, predicted_classes = test_prog()

def classes_accuracy(real_classes, predicted_classes):
    '''
    Provides a really basic measure of accuracy by comparing the real and predicted class for each pixel

    Parameters
    ----------
    real_classes : 2-D ARRAY
        An array with the classes attributed to each pixel of the original image.
    predicted_classes : 2-D ARRAY
        An array with the classes predicted for each pixel of the original image.

    Returns
    -------
    FLOAT
        The accuracy measure between 0 and 1.

    '''
    height_img, width_img = real_classes.shape
    error = 0

    for i in range(height_img) :
        for j in range(width_img) :
            if (real_classes[i][j] != predicted_classes[i][j]) :
                error += 1

    #We return a measure of the accuracy, knowing that the error is between 0 and heigh*width
    return (1- error /(height_img * width_img))


def visual_discrepancy(real_classes, predicted_classes, criterion = 0.5, nb_classes = 16):
    '''
    Provides a rough accuracy measure by comparing how "smooth" the predicted image is
    compared to the original one, that is how many perturbations there are in global
    regions composing the image

    Parameters
    ----------
    real_classes : 2-D ARRAY
        An array with the classes attributed to each pixel of the original image.
    predicted_classes : 2-D ARRAY
        An array with the classes predicted for each pixel of the original image.
    criterion : INT
        The required smoothness for identifying regions in an image. The default is 0.8
    nb_classes : INT, optional
        Number of color classes used. The default is 16.

    Returns
    -------
    FLOAT
        A number between 0 (if the smoothnesses are the same) and 1 (if they
        are not similar at all.)

    '''
    height_img, width_img = real_classes.shape

    smoothness, psmoothness = 0, 0

    regions = split(real_classes, criterion, nb_classes)
    pregions = split(predicted_classes, criterion, nb_classes)
    
    og_count, pred_count = 0,0
    #Computing smoothnesses
    for list_reg in regions:
        list_reg = flatten(list_reg)
        for re in list_reg :
            smoothness += smooth(re, nb_classes)
            og_count +=1
            
    for list_preg in pregions:
        list_preg = flatten(list_preg)
        for re in list_preg :
            psmoothness += smooth(re, nb_classes)
            pred_count += 1
    diff_segmentation = (pred_count - og_count) / (height_img * width_img -1) #score up to 1 measuring discrepancy between pictures separation into regions

    return abs(psmoothness-smoothness * diff_segmentation) / (height_img * width_img) #up to 1 


def split(list_regions, criterion = 0.5, nb_classes = 16):
    '''
    Recursive function which divides regions into 4 regions of about the same size while
    they do not meet a uniformity criterion.

    Parameters
    ----------
    list_regions : NUMPY NDARRAY
        A list containing the regions that might have to be splitted.
    criterion : FLOAT
        The required smoothness expected from a region in order not to be splitted (see smooth function).
    nb_classes : INT
        Number of different classes in the images, default is 16
    
    Returns
    -------
    LIST
        A nested list containing the splitted regions.

    '''
    input_list = list_regions.copy()
    #print("List is",input_list)

    res = []
    height, width = input_list.shape

    #print(height,width)
        
    #Return none for empty arrays, filtered out of result afterwards
    if ((height == 0) or (width == 0)):
        #print("height width")
        return None
    
    #Single pixels
    if (height == 1) and (width ==1):
        return input_list
    
    #If the region is smooth enough we stop
    if (smooth(input_list)>= criterion):
        return input_list
    
    #Else we continue to split it
    vmid, hmid = height//2, width//2
    r1 = input_list[0:vmid,0:hmid]
    r2 = input_list[0:vmid,hmid:width]
    r3 = input_list[vmid:height,0:hmid] 
    r4 = input_list[vmid:height,hmid:width]
        
    #Try to split the four subregions and add them
    if(len(r1)):
        res.append(split(r1))
    if(len(r2)):
        res.append(split(r2))
    if(len(r3)):
        res.append(split(r3))
    if(len(r4)):
        res.append(split(r4))
    
    return(res)
    

def smooth(region, nb_classes = 16):
    '''
    Returns a measure of the region smoothness

    Parameters
    ----------
    region : LIST
        A list containing the region. Must be unnested thanks to flatten.
    nb_classes : TYPE, optional
        DESCRIPTION. The default is 16.

    Returns
    -------
    FLOAT
        A number between 0 and 1, representing the proportion of the region taken by the majority class.

    '''
    height, width = region.shape
    classes = np.zeros(nb_classes)
    for h in range(height):
        for w in range(width):
            element = region[h][w]
            classes[element] += 1
    majority = np.max(classes)
    #print('Smoothness : ',majority/(height*width))
    #Returns a proportion up to 1 representing the "visual homogeneity"
    return majority / (height * width) 
 
def flatten(regions_list):
    '''
    Unnest a list recursively in order to compute smoothness

    Parameters
    ----------
    regions_list : LIST
        A list containing the arrays which represent different regions.

    Returns
    -------
    LIST
        The same list unnested (with all the arrays as simple list elements).

    '''
    if len(regions_list) == 0 :
        return regions_list
    if isinstance(regions_list[0], list):
        return flatten(regions_list[0]) + flatten(regions_list[1:])
    return regions_list[:1] + flatten(regions_list[1:])

def show_measures(iterations = 2):
    basic_error = []
    smoothness_error = []
    
    for i in range(iterations):
        real_classes, predicted_classes = test_prog()
        basic_error.append(1- classes_accuracy(predicted_classes,real_classes))
        smoothness_error.append(visual_discrepancy(real_classes, predicted_classes))

    plt.figure()
    
    plt.boxplot([basic_error,smoothness_error])
    
    plt.gca().xaxis.set_ticklabels(['Basic', 'Smoothness method'])
    plt.title("Comparison of error indicators over " + str(iterations) +" iterations")

def show_classes(real_classes, predicted_classes, dim = (150,200), denormalization = 1):
    '''
    

    Parameters
    ----------
    real_classes : 2-D ARRAY
        An array with the classes attributed to each pixel of the original image.
    predicted_classes : 2-D ARRAY
        An array with the classes predicted for each pixel of the original image.
    dim : INT TUPLE, optional
        The size desired for the displayed image. The default is (150,200).
    denormalization : INT, optional
        Factor distributing the classes on a bigger scale for image representation. The default is 1.

    Returns
    -------
    None.

    '''
    real_classes = cv2.resize(real_classes,dim)
    predicted_classes = cv2.resize(predicted_classes,dim)
    cv2.imshow('Real', denormalization * real_classes)
    cv2.imshow('Predicted', denormalization * predicted_classes)
    cv2.waitKey()

