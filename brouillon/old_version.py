from scipy.spatial import distance # you can select the euclidean distance
from scipy import stats #
import sys
import numpy as np
from matplotlib import pyplot as plt
import cv2
from sklearn.cluster import KMeans
from scipy.spatial import distance #Scipy is very fast to compute the distance between two vectors

image = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')
#image = cv2.imread(r'.\IMG_0062.jpg')
#image=image[0:99,0:99]
img_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)#Convertir en gris
#cv2.imshow(image)

height, width = img_grey.shape

pixels_img_grey = np.zeros((height * width))
color = np.zeros((height, width))
pixels_img = [image[i,j] for i in range(height) for j in range(width)]

#Kmeans algo to classify each RGB pixel of a color image into one of the 16 classes
kmeans = KMeans(16)
pixels_imgNew = kmeans.fit_predict(pixels_img)

#Transformation in an appropraite data structure if needed
for i in range(height):
    for j in range(width):
        color[i][j] = pixels_imgNew[i*width + j]
        pixels_img_grey[i*width + j] = img_grey[i][j]

"""
###### INTERESTING FUNCTIONS ########
#Discrete Fourier Transformation magnitud
f = np.fft.fft2(img_grey) #Discrete Fourier Transform
fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
magnitude_spectrum = 20*np.log(np.abs(fshift))

#SIFT descriptors
#surf = cv2.xfeatures2d.SURF_create(400) # Create SURF object. Hessian Threshold to 400
#kp, des = surf.detectAndCompute(img_grey, None) # Find keypoints and descriptors directly
sift = cv2.SIFT_create() # Create SIFT object
kp, descriptors = sift.detectAndCompute(img_grey, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly

# localized mean and standard di=eviation of the intensity
mean = np.average(img_grey)
deviation = np.std(img_grey)
#print(mean, deviation)

# grayscale histogram
grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
"""

class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, K):
        '''
        INPUT :
        - K : is a natural number bigger than 0
        '''
        # empty initialization of X and y
        self.X = []
        self.y = []
        # K is the parameter of the algorithm representing the number of neighborhoods
        self.k = K

    def train(self,X,y):
        '''
        INPUT :
        - X : is a 2D Nx4 numpy array containing the features
        - y : is a 1D Nxk or N numpy array containing the colors
        '''
        self.X=X.copy() # copy your training points
        self.y=y.copy()

    def predict(self,X_new):
        '''
        INPUT :
        - X_new : is a Mx4 numpy array containing the features of new pixels whose label has to be predicted

        OUTPUT :
        - y_hat : is a Mx1 numpy array containing the predicted labels for the X_new points
        '''
        y_hat = []
        for x in X_new:
            distance = []
            voisin = []
            for p in self.X :
                distance.append((x - p)**2)
            for i in range(self.k):
                v = np.argmin(distance)
                distance.pop(v)
                voisin.append(self.y[v])
            y_hat.append((1/len(voisin)*sum(voisin)))

        return y_hat

    def predict_bis(self,X_new):
        '''
        INPUT :
        - X_new : is a numpy array containing the features of new pixels whose label has to be predicted

        OUTPUT :
        - y_hat : is a Mx1 numpy array containing the predicted labels for the X_new points
        '''
        dst = []
        voisin = []

        for i in range(height) :
            for j in range(width) :
                dst.append(distance.euclidean(self.X[i,j], X_new))

        for i in range(self.k):
            v = np.argmin(dst)
            dst.pop(v)
            voisin.append(self.y[v//height, v % heidth])

        y_hat = (1/len(voisin)*sum(voisin))

        return y_hat

"""Additional features"""
"""delta=11
Xtra_features=np.zeros((height, width, 763))

for i in range(height) :
    for j in range(width) :

        radius=delta//2
        if (radius<=i<=height-radius) and (radius<=j<=width-radius) :
            window=img_grey[i-radius+1: i+radius, j-radius: j+radius+1]

        else :
            window=img_grey[(i//delta)*delta: (i//delta)*delta+delta, (j//delta)*delta: (j//delta)*delta+delta]

        f = np.fft.fft2(window) #Discrete Fourier Transform
        fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
        magnitude_spectrum = 20*np.log(np.abs(fshift))
        Xtra_features[i,j,:121]=np.array(magnitude_spectrum).ravel()

        grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
        Xtra_features[i,j, 121:377]=np.array(grayscale_hist).ravel()

        mean = np.average(window)
        deviation = np.std(window)
        Xtra_features[i,j,377]=mean
        Xtra_features[i,j,378]=deviation
        """


def knn_per_feature():
    delta=11

    x_magnitudeSpectrum = np.zeros((height, width, 121))
    x_grayHist = np.zeros((height, width, 256))
    x_mean = np.zeros((height, width))
    x_deviation = np.zeros((height, width))
    x_descriptors = np.zeros((height, width))

    for i in range(height) :
        for j in range(width) :
            radius=delta//2
            if (radius<=i<=height-radius) and (radius<=j<=width-radius) :
                window=img_grey[i-radius+1: i+radius, j-radius: j+radius+1]

            else :
                window=img_grey[(i//delta)*delta: (i//delta)*delta+delta, (j//delta)*delta: (j//delta)*delta+delta]

            f = np.fft.fft2(window) #Discrete Fourier Transform
            fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
            magnitude_spectrum = 20*np.log(np.abs(fshift))
            x_magnitudeSpectrum[i,j]=np.array(magnitude_spectrum).ravel()

            grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
            x_grayHist[i,j]=np.array(grayscale_hist).ravel()

            mean = np.average(window)
            deviation = np.std(window)
            x_mean[i,j]=mean
            x_deviation[i,j]=deviation

            """sift = cv2.SIFT_create() # Create SIFT object
            kp, descriptors = sift.detectAndCompute(window, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly
            x_descriptors[i,j] =  descriptors""" #Problem of size

    knn = KNN(K=5)
    knn.X = pixels_img_grey[0:200]
    knn.y = pixels_imgNew[0:200]
    rep_grey = knn.predict(pixels_img_grey[0:200])

    knn = KNN(K=5)
    knn.X = x_magnitudeSpectrum
    knn.y = color
    rep_magnitudeSpectrum = knn.predict_bis(pixels_img_grey[0:200])

    knn = KNN(K=5)
    knn.X = x_grayHist
    knn.y = color
    rep_grayHist = knn.predict(pixels_img_grey[0:200])

    knn = KNN(K=5)
    knn.X = x_deviation
    knn.y = color
    rep_deviation = knn.predict(pixels_img_grey[0:200])

    knn = KNN(K=5)
    knn.X = x_mean
    knn.y = color
    rep_mean = knn.predict(pixels_img_grey[0:200])







knn = KNN(K=5)
knn.X = pixels_img_grey[0:200]
knn.y = pixels_imgNew[0:200]
rep = knn.predict(pixels_img_grey[0:200])
print(rep, pixels_imgNew[0:200])
