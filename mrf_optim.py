import numpy as np
import cv2

img = cv2.imread('IMG_0062.jpg')

"""
Source : Stackoverflow

The gradient of a function of two variables x, y is a vector of the partial derivatives in the x and y direction. So if your function is f(x,y), the gradient is the vector (f_x, f_y). An image is a discrete function of (x,y), so you can also talk about the gradient of an image.

The gradient of the image has two components: the x-derivative and the y-derivative. So, you can think of it as vectors (f_x, f_y) defined at each pixel. These vectors have a direction atan(f_y / fx) and a magnitude sqrt(f_x^2 + f_y^2). So, you can represent the gradient of an image either an x-derivative image and a y-derivative image, or as direction image and a magnitude image.
"""
def img_pixel(img):
    class_to_pixels = np.array([[12, 35, 67], [123, 56, 136], [20, 156, 45]]).astype(int)   #### for testing !!!
    height, width = img.shape
    img_bis = np.zeros((height, width, 3))
    for i in range(height):
        for j in range(width):
            index = int(img[i,j])
            img_bis[i,j] = class_to_pixels[index]
    return img_bis.astype(dtype =np.uint8)

def magnitude_gradient(img_bis):
    """
    Gives the magnitude of the gradient at each point of the image

    Parameters
    ----------
    img_bis : NUMPY ARRAY
        Image as an array.

    Returns
    -------
    magnitude : NUMPY ARRAY
        Magnitude values of the image gradient.

    """
    
    img = img_pixel(img_bis)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #Convert to grey
    kernely = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
    kernelx = np.array([[1,0,-1],[1,0,-1],[1,0,-1]])
    f_x = cv2.filter2D(img,cv2.CV_8U,kernelx)
    f_y = cv2.filter2D(img,cv2.CV_8U,kernely)

    magnitude = np.sqrt(f_x ** 2 + f_y ** 2)

    #cv2.imshow('Gradients_X',f_x)
    #cv2.imshow('Gradients_Y',f_y)
    #print(magnitude)
    return magnitude


#Graph Modelisation
###ALL THIS PART IS BASED ON THE FOLLOWING PAPER
# http://www.cs.cornell.edu/rdz/Papers/BVZ-iccv99.pdf

def alpha_beta_graph(alpha, beta, f, L):
    """
    Creates a graph designed for the alpha-beta swap method

    Parameters
    ----------
    alpha : INT
        First color class.
    beta : int
        Second color class.
    f : 2-D ARRAY
        Initial labeling associating pixels with color classes.
    L : 2-D ARRAY
        Matrix with the color probabilities of each pixel.

    Returns
    -------
    graph : 2-D ARRAY
        Contains the vertices used in the alpha-beta swap method.
    weight : 2-D ARRAY
        Weights of edges between vertices (integers or infinite values).

    """
    height, width = f.shape
    magnitude = magnitude_gradient(f)
    graph = np.zeros((height, width))
    vertices = np.zeros((height, width))
    weight = np.zeros((height*width + 2, width*height + 2)) #2D matrix wher [i,j] contains the weight of the link from i to j
   
    for i in range(height-1):
        for j in range(width-1):
            if f[i,j] == alpha :
                alpha_weight = smooth_cost(alpha, i, j, vertices, f, magnitude)             
                vertices[i,j] = 1
                graph[i,j] = 1 #when 1, the pixel [i,j] is in the graph we want to create
                weight[0, i*width + j] = alpha_weight
                weight[i*width + j, 0] = alpha_weight
                
            if f[i,j] == beta :
                beta_weight = smooth_cost(beta, i, j, vertices, f, magnitude)
                vertices[i,j] = 1
                graph[i,j] = 1
                weight[0, i*width + j] = beta_weight
                weight[i*width + j, 1] = beta_weight
                
                
    for i in range(height):
        val = f[i,width-1]
        if (val == alpha) or (val == beta):
            vertices[i,j] = 1
            graph[i,j] = 1
                
    for j in range(width):
        val = f[height-1,j]
        if (val == alpha) or (val == beta):
            vertices[height-1,j] = 1
            graph[height-1,j] = 1 
    
    val = beta-alpha                
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            if vertices[i,j] :
                
                for idv in range(-1,2):
                    for idh in range(-1,2):
                        if not(idv) and not(idh):
                            continue
                        if vertices[i+idv,j+idh]:
                            weight[(i+idv)*width + j+idh +2, i*width + j +2] = val
                            weight[i*width + j +2, (i+idv)*width + j+idh +2] = val

    return graph, weight

def alpha_graph(alpha, f, L):
    """
    Creates a graph designed for the alpha-expansion method

    Parameters
    ----------
    alpha : INT
        Color class used in the alpha-expansion.
    f : 2-D ARRAY
        Initial labeling associating pixels with color classes.
    L : 2-D ARRAY
        Image with values divided by 256.

    Returns
    -------
    graph : 2-D ARRAY
        Contains the vertices used in the alpha-expansion method.
    weight : 2-D ARRAY
        Weights of edges between vertices (integers or infinite value).

    """
    height, width = f.shape
    magnitude = magnitude_gradient(f)
    graph = np.zeros((height, width))
    vertices = np.zeros((height, width))
    
    aux_count = auxiliaries(f)
    
    weight = np.zeros((height*width + 2 + aux_count, width*height + 2 + aux_count))
    #vertices are alpha, "non-alpha", image pixels and auxiliaries
    
    for i in range(height):
        for j in range(width):
            if f[i,j] == alpha :
                vertices[i,j] = 1
                graph[i,j] = 1 

    for i in range(height-1):
        for j in range(width-1):
            if f[i,j] == alpha :
                weight[1, i*width + j] = float('inf') #pseudo-infinite value
                weight[i*width + j, 1] = float('inf')
            else:
                dp_fp = (magnitude[i][j] - f[i][j]) **2
                weight[1, i*width + j] = dp_fp
                weight[i*width + j, 1] = dp_fp
    
    count = 0
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            value = f[i,j]
            diff_alpha = abs(alpha - value)
            
            for idv in range(-1,2):
                for idh in range(-1,2):
                    if not(idv) and not(idh):
                        continue
                    if f[i+idv,j+idh] == value:
                        weight[(i+idv)*width + j+idh +2, i*width + j +2] = diff_alpha
                        weight[i*width + j +2, (i+idv)*width + j+idh +2] = diff_alpha
                    else:
                        weight[i*width + j +2, width+count] = abs(value-alpha)
                        weight[height + count, i*width + j +2] = abs(value-alpha)
                        
                        weight[(i+idv)*width + (j+idh) +2, width+count] = abs(f[i+idv,j+idh]-alpha)
                        weight[height + count, (i+idv)*width + (j+idh) +2] = abs(f[i+idv,j+idh]-alpha)
                        
                        weight[1, width+count] = abs(value-f[i+idv,j+idh])
                        weight[height+count,1] = abs(value-f[i+idv,j+idh])
                        
                        count +=1
    
    return graph, weight

def auxiliaries(f):
    """
    Finds out how many auxiliaries are needed in the alpha-expansion

    Parameters
    ----------
    f : 2-D ARRAY
        Initial labeling associating pixels with color classes.

    Returns
    -------
    count : INT
        Number of auxiliaries.

    """
    height, width = f.shape
    
    count = 0
    for i in range(1, height - 1):
        for j in range(1, width - 1):
            value = f[i,j]
            val_neigh = f[i-1:i+1, j-1:j+1].flatten() #8 neighbors
            for elem in val_neigh:
                if (elem != value):
                    count +=1
        
    return count


def neighbors(graph): 
    """
    Computes the neighbors of each vertex

    Parameters
    ----------
    graph : 2-D ARRAY
        Contains the vertices used in the algorithms.

    Returns
    -------
    neighbors : 2-D ARRAY
        Contains the tuples (ID , vertical coordinate, horizontal coordinate) of a neighbor.
    neighbors_source : 2-D ARRAY
        Contains the tuples (ID , vertical coordinate, horizontal coordinate) of the vertices tested.

    """
    # each pixel is a neighbor of alpha and beta and of its 8 neighbors in the picture which belong to the graph
    
    height, width = graph.shape
    neighbors = -1 * np.ones((height * width, 8, 3)) #the 3D array of neighbors contain for each pixel the coordinates of its neighbors
    neighbors_source = []
    if graph[0, 0] :                                 #we treat differently the 4 corners and the 4 margins of the picture
        neighbors_source.append([2,0,0])
        if graph[0, 1] :
            neighbors[0][0] =  [3, 0, 1]
        if graph[1, 0] :
            neighbors[0][1] =  [width  +2, 1, 0]
        if graph[1, 1] :
            neighbors[0][2] =  [width +1 +2, 1, 1]
    if graph[height - 1, 0] :
        neighbors_source.append([(height - 1)*width +2, height - 1,0])
        if graph[height - 1, 1] :
            neighbors[height - 1][0] =  [(height-1) * width +1 +2, height - 1, 1]
        if graph[height - 2, 0] :
            neighbors[height - 1][1] =  [(height-2) * width  +2, height - 2, 0]
        if graph[height - 2, 1] :
            neighbors[height - 1][2] =  [(height-2) * width +1 +2, height - 2, 1]
    if graph[0, width-1] :
        neighbors_source.append([width-1 + 2,0,width-1])
        if graph[0, width-2] :
            neighbors[width - 1][0] =  [0*width + width-2 +2, 0, width-2]
        if graph[1, width-1] :
            neighbors[width - 1][1] =  [1*width + width-1  +2, 1, width-1]
        if graph[1, width-2] :
            neighbors[width - 1][2] =  [1*width + width-2 +2, 1, width-2]
    if graph[height-1, width-1] :
        neighbors_source.append([(height-1)*width + width-1 +2, height-1, width-1])              #################### doubt size
        if graph[height-1, width-2] :
            neighbors[height * width -1][0] =  [(height-1) * width + width-2 +2, height-1, width-2]
        if graph[height-2, width-1] :
            neighbors[height * width -1][1] =  [(height-2) * width + width-1  +2, height-2, width-1]
        if graph[height-2, width-2] :
            neighbors[height * width -1][2] =  [(height-2) * width + width-2 +2, height-2, width-2]

    for i in range(1, height - 1):
        if graph[i, 0] :
            neighbors_source.append([i*width +2, i, 0])
            if graph[i-1, 0] :
                neighbors[i*width][0] =  [(i-1)*width +2, i-1, 0] ####################"doubt
            if graph[i+1, 0] :
                neighbors[i*width][1] =  [(i+1)*width +2, i+1, 0]
            if graph[i-1, 1] :
                neighbors[i*width][2] =  [(i-1)*width +1 +2, i-1, 1]
            if graph[i, 1] :
                neighbors[i*width][3] =  [i*width + 1 +2, i, 1]
            if graph[i+1, 1] :
                neighbors[i*width][4] =  [(i+1)*width +1 +2, i+1, 1]
    for i in range(1, height - 1):
        if graph[i, width - 1] :
            neighbors_source.append([i*width + width - 1 +2, i, height - 1])
            if graph[i-1, width - 1] :
                neighbors[i*width + width - 1][0] =  [(i-1)*width + width-1 +2, i-1, width-1] ####################"doubt
            if graph[i+1, width - 1] :
                neighbors[i*width + width - 1][1] =  [(i+1)*width + width-1 +2, i+1, width-1]
            if graph[i-1, width - 2] :
                neighbors[i*width + width - 1][2] =  [(i-1)*width + width-2 +2, i-1, width-2]
            if graph[i, width - 2] :
                neighbors[i*width + width - 1][3] =  [i*width + width-2 +2, i, width-2]
            if graph[i+1, width - 2] :
                neighbors[i*width + width - 1][4] =  [(i+1)*width + width-2 +2, i+1, width-2]
    for j in range(1, width - 1):
        if graph[0, j] :
            neighbors_source.append([j +2, 0, j])
            if graph[0, j-1] :
                neighbors[j][0] =  [j-1 +2, 0, j-1] ####################"doubt
            if graph[0, j+1] :
                neighbors[ j][1] =  [j+1 +2, 0, j+1]
            if graph[1, j-1] :
                neighbors[ j][2] =  [j-1 +2, 1, j-1]
            if graph[1, j] :
                neighbors[ j][3] =  [j +2, 1, j]
            if graph[1, j+1] :
                neighbors[ j][4] =  [j+1 +2, 1, j+1]
    for j in range(1, width - 1):
        if graph[height - 1, j] :
            neighbors_source.append([(height - 1)*width +j +2, 0, j])
            if graph[height - 1, j-1] :
                neighbors[(height-1)*width + j][0] =  [(height - 1)*width +j-1 +2, height - 1, j-1] ####################"doubt
            if graph[height - 1, j+1] :
                neighbors[(height-1)*width + j][1] =  [(height - 1)*width +j+1 +2, height - 1, j+1]
            if graph[height - 2, j-1] :
                neighbors[(height-1)*width + j][2] =  [(height - 2)*width +j-1 +2, height - 2, j-1]
            if graph[height - 2, j] :
                neighbors[(height-1)*width + j][3] =  [(height - 2)*width +j +2, height - 2, j]
            if graph[height - 2, j+1] :
                neighbors[(height-1)*width + j][4] =  [(height - 2)*width +j+1 +2, height - 2, j+1]

    for i in range(1, height - 1):
        for j in range(1, width - 1):
            if graph[i, j] :
                neighbors_source.append([i*width + j +2, i, j])
                 
                count = 0
                for idv in range(-1,2):
                    for idh in range(-1,2):
                        if not(idv) and not(idh):
                            continue
                        if graph[i+idv, j+idh]:
                            neighbors[i*width +j][count] = [(i+idv)* width + j + idh +2, i+idv, j+idh]
                            count +=1
                
    return neighbors, neighbors_source

###NEIGHBORS CASE 1

def smooth_cost(alpha,i,j,vertices,label, magnitude):
    """
    Computes the weight of an edge between a color class vertex and the vertex at (i,j)

    Parameters
    ----------
    alpha : INT
        One of the color classes.
    i : INT
        Vertical coordinate.
    j : INT
        Horizontal coordinate.
    vertices : 2-D ARRAY
        Contains the vertices used in the alpha-beta swap method.
    label : 2-D ARRAY
        Labeling of the vertices.
    magnitude : 2-D ARRAY
        Magnitude of the gradient at each pixel.

    Returns
    -------
    The edge weight.

    """
    for idv in range(-1,2):
        for idh in range(-1,2):
            if not(idv) and not(idh):
                continue
            if not(vertices[i+idv, j+idh]) :
                return(magnitude[i+idv, j+idh] * (label[i+idv, j+idh] - alpha))

def edmonds_karp(graph, weight):
    """
    Edmonds-Karp algorithm (computes the maximal flow in a graph)

    Parameters
    ----------
    graph : 2-D ARRAY
        Contains the vertices used in the algorithms.
    weight : 2-D ARRAY
        Weights of edges between vertices (integers or infinite values).

    Returns
    -------
    flow : INT
        Maximal graph flow.
    F : 2-D ARRAY
       Matrix of flows.

    """
    
    # source, destination = 0, 1
    height, width = graph.shape
    neighbours, neighbors_source = neighbors(graph)
    flow = 0 # maximal flow
    F = np.zeros((height*width + 2, width*height + 2))#matrix of flows
    capacity, Parents = breadth_first_search(weight, neighbours, neighbors_source, F)
    while capacity != 0:
        capacity, Parents = breadth_first_search(weight, neighbours, neighbors_source, F)
        flow += capacity
        v = 1
        while v != 0 :
            u = Parents[v]
            F[u,v] = F[u,v] + capacity
            F[v,u] = F[v,u] - capacity
            v = u

    return flow, F

def breadth_first_search(weight, neighbors, neighbors_source, F):
    """
    Performs a breadth first search of the graph

    Parameters
    ----------
    weight : 2-D ARRAY
        Weights of the graph edges.
    neighbors : 2-D ARRAY
        Contains the tuples (ID , vertical coordinate, horizontal coordinate) of a neighbor.
    neighbors_source : 2-D ARRAY
        Contains the tuples (ID , vertical coordinate, horizontal coordinate) of the vertices to which the neighbors refer.
    F : 2-D ARRAY
        Matrix of flows from Edmonds-Karp algorithm.

    Returns
    -------

    Parents : 2-D ARRAY
        Parent of a vertex.

    """
    height, width = weight.shape
    Parents = -1  * np.ones((height, width))
    capacity = np.zeros((height * width))
    Parents[0] = -2
    capacity[0] = float('inf')  #infinity
    q = [0]
    while len(q) > 0:
        u = q.pop(0)
        if u == 0 or u == 1 :
            for v in neighbors_source:
                #print(v, neighbors_source)
                if weight[u,int(v[0])]- F[u,int(v[0])] > 0 and Parents[int(v[1]), int(v[2])] == -1 :
                    Parents[int(v[1]),int(v[2])] = u
                    capacity[int(v[0])] = min(capacity[u], weight[u,int(v[0])]- F[u,int(v[0])])
                    if v[0] != 1:
                        q.append(int(v[0]))
                    else:
                        return capacity[1], Parents
        else :
            for v in neighbors[u]:
                if v[0] != -1:
                    #print("v0 =", v[0])
                    #print()
                    if weight[u,int(v[0])]- F[u,int(v[0])] > 0 and Parents[int(v[1]), int(v[2])] == -1 :
                        Parents[int(v[1]),int(v[2])] = u
                        capacity[int(v[0])] = min(capacity[u], weight[u,int(v[0])]- F[u,int(v[0])])
                        if v[0] != 1:
                            q.append(int(v[0]))
                        else:
                            return capacity[1], Parents
    return 0, Parents


def breadth_first_search_from_source(F, weight):
    """
    Performs a BFS from the graph source

    Parameters
    ----------
    F : 2-D ARRAY
        Matrix of flows from Edmonds-Karp algorithm.
    weight : 2-D ARRAY
        Weights of the graph edges.

    Returns
    -------
    marked : ARRAY
        Contains vertices found in the BFS.

    """
    Res = weight - F
    f = [0]
    marked = [0]
    while len(f) > 0:
        s = f.pop(len(f)-1)
        for i in range(len(Res)):
            if Res[s, i] != 0 and marked.count(i) == 0 :
                f.append(i)
                marked.append(i)
    return marked



def minimum_graph_cut(graph, weight):
    """
    Finds a minimal cut in the given graph

    Parameters
    ----------
    graph : 2-D ARRAY
        Contains the vertices used in the algorithms.
    weight : 2-D ARRAY
        Weights of the graph edges.

    Returns
    -------
    S : 2-D ARRAY
        Vertices marked to 1 are with the source in the cut.
    T : 2-D ARRAY
        Vertices marked to 1 are not with the source in the cut.

    """
    flow, F = edmonds_karp(graph, weight)
    marked = breadth_first_search_from_source(F, weight)
    height, width = graph.shape
    S = np.zeros((height+2, width+2))
    T = np.zeros((height+2, width+2))
    S[0,0] = 1
    T[1,1] = 1
    for i in range(height):
        for j in range(width):
            if graph[i,j] :
                if not(marked.count(i*width + j)) :
                    T[i+2,j+2] = 1
                else :
                    S[i+2,j+2] = 1
    return S, T

def minimal_labeling(graph, S, T, label, alpha, beta):
    """
    Applies the alpha-beta swap method to find a labeling minimizing the energy function

    Parameters
    ----------
    graph : 2-D ARRAY
        Contains the vertices used in the algorithms.
    S : 2-D ARRAY
        Vertices marked to 1 are with the source in the cut.
    T : 2-D ARRAY
        Vertices marked to 1 are not with the source in the cut.
    label : 2-D ARRAY
        Initial labeling of the vertices.
    alpha : INT
        First color class.
    beta : int
        Second color class.

    Returns
    -------
    label_bis : 2-D ARRAY
        Labeling found by the method.

    """
    label_bis = label
    height, width = graph.shape
    for i in range(height):
        for j in range(width):
            if graph[i,j] :
                if S[0,0] and T[i+2, j+2] :
                    label_bis[i,j] = alpha

                elif T[1,1]  and S[i+2, j+2] :
                    label_bis[i,j] = beta
                #else: keep the previous label
    return label_bis

def miniswap_labeling(graph, S, T, label, alpha):
    """
    Applies the alpha-expansion method to find a labeling minimizing the energy function

    Parameters
    ----------
    graph : 2-D ARRAY
        Contains the vertices used in the algorithms.
    S : 2-D ARRAY
        Vertices marked to 1 are with the source in the cut.
    T : 2-D ARRAY
        Vertices marked to 1 are not with the source in the cut.
    label : 2-D ARRAY
        Initial labeling of the vertices.
    alpha : INT
        Color class.

    Returns
    -------
    label_bis : 2-D ARRAY
        Labeling found by the method.

    """
    label_bis = label
    height, width = graph.shape
    for i in range(height):
        for j in range(width):
            if graph[i,j] :
                if S[0,0] and T[i+2, j+2] :
                    label_bis[i,j] = alpha
                #else: keep the previous label
    return label_bis

def label_costs(i,j, label, L, K):
    """
    Computes the cost associated to giving the specified label to the vertex (i,j)

    Parameters
    ----------
    i : INT
        Vertical coordinate.
    j : INT
        Horizontal coordinate.
    label : TYPE
        DESCRIPTION.
    L : 2-D ARRAY
        Matrix with the color probabilities of each pixel.
    K : TYPE
        DESCRIPTION.

    Returns
    -------
    label_cost : INT
        Cost associated with the label value at the given vertex.

    """
    label_cost = 0
    for k in range(len(K)):
        label_cost += L[i, j, k] * (k == label[i,j])
    return label_cost



def energy_function(label, L, K):
    """
    The energy to minimize in the algorithms

    Parameters
    ----------
    label : 2-D ARRAY
        Initial labeling of the vertices.
    L : 2-D ARRAY
        Matrix with the color probabilities of each pixel.
    K : ARRAY
        Contains the color classes values.

    Returns
    -------
    pixel_energy : INT
        Value of the energy function for all pixels.

    """
    magnitude = magnitude_gradient(label)
    pixel_energy = 0
    height, width, u = L.shape
    for i in range(1, height - 1):
        for j in range(1, width - 1):

            lij = label[i,j]
            for idv in range(-1,2):
                for idh in range(-1,2):
                    if not(idv) and not(idh):
                        continue
                    pixel_energy += magnitude[i+idv, j+idh] * (label[i+idv,j+idh] - lij)     

            pixel_energy -= label_costs(i,j, label, L, K)
    return pixel_energy



def graph_cut(K, L):
    """
    Applies the alpha-beta swap algorithm to the graph

    Parameters
    ----------
    K : ARRAY
        Contains the color classes values.
    L : 2-D ARRAY
        Matrix with the color probabilities of each pixel.

    Returns
    -------
    label : 2-D ARRAY
        Initial labeling of the vertices.

    """
    #start with an arbitrary labeling label
    #2 set success = 0
    # For each pair of labels {α, β} ⊂ K color classes ensemble
    # 3   Find ˆf = arg min E(f′) among f ′ withinone α-β swap of f
    # 4    If E( ˆf ) < E(f ), set f := ˆf and success := 1
    #If success = 1 goto 2
    height, width, u = L.shape
    label = np.random.randint(len(K), size=(height, width))
    #print(label)
    success = 1
    while success :
        success = 0
        for i in range(0, len(K)-1):
            for j in range(i+1, len(K)):
                alpha = K[i]
                beta = K[j]
                graph, weight = alpha_beta_graph(alpha, beta, label, L)
                S, T = minimum_graph_cut(graph, weight)
                label_bis = minimal_labeling(graph, S, T, label, alpha, beta)
                if (energy_function(label_bis, L, K) < energy_function(label, L, K)):
                    label = label_bis
                    success = 1

    return label

def swap_cut(K,L):
    """
    Applies the alpha-expansion algorithm to the graph

    Parameters
    ----------
    K : ARRAY
        Contains the color classes values.
    L : 2-D ARRAY
        Matrix with the color probabilities of each pixel.

    Returns
    -------
    label : 2-D ARRAY
        Initial labeling of the vertices.

    """
    height, width, u = L.shape
    label = np.random.randint(len(K), size=(height, width))
    success = 1
    while success :
        success = 0
        for i in range(0, len(K)-1):
                alpha = K[i]
                graph, weight = alpha_graph(alpha, label, L)
                S, T = minimum_graph_cut(graph, weight)
                label_bis = miniswap_labeling(graph, S, T, label, alpha)
                if (energy_function(label_bis, L, K) < energy_function(label, L, K)):
                    label = label_bis
                    success = 1

    return label


def test():
    image = cv2.imread('IMG_0062.jpg')
    #cv2.imshow('',image)
    #cv2.waitKey()
    image = image[:10, :10]
    #print(image)
    #print()
    #print()
    #cv2.imshow('',image)
    #cv2.waitKey()
    #magnitude_gradient(image)
    K = [0,1,2]
    L = image/256
    a = graph_cut(K,L)
    #print(a)
    #cv2.imshow('',a)
    #cv2.waitKey()
    return(a)





