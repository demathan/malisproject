from scipy.spatial import distance # you can select the euclidean distance
from scipy import stats #
import sys
import numpy as np
from matplotlib import pyplot as plt
import cv2
from sklearn.cluster import KMeans
from scipy.spatial import distance #Scipy is very fast to compute the distance between two vectors
import math
import random


def select_random(img): #selects random pixels that accounts to nearly 2% of the image
    height, width, p = img.shape
    h = math.floor(height / 10)
    w = math.floor(width / 10)
    img_bis = np.zeros((h,w,3))
    for i in range(h):
        for j in range(w):
            i_bis = random.randrange(height)
            j_bis = random.randrange(width)
            img_bis[i,j] = img[i_bis, j_bis]
    return img_bis.astype(dtype =np.uint8)
    
def preparation():
    #image = cv2.imread('C:\\Users\\jacqu\\Documents\\bertrand.jpg')
    image = cv2.imread(r'.\IMG_0062.jpg')
    #image = image[:32, :24]
    image = select_random(image)
    
    img_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)#Convertir en gris
    #cv2.imshow('',image)
    
    height, width = img_grey.shape
    print(height, width)
    pixels_img_grey = np.zeros((height * width))
    color = np.zeros((height, width))
    pixels_img = [image[i,j] for i in range(height) for j in range(width)]
    
    #Kmeans algo to classify each RGB pixel of a color image into one of the 16 classes
    kmeans = KMeans(16)
    # Compute cluster centers and predict cluster index for each sample
    pixels_img_km = kmeans.fit_predict(pixels_img)
    # RGB coordinates of cluster centers 

    pixels_imgNew = kmeans.cluster_centers_ 
    
    cluster_centers = kmeans.cluster_centers_ 
    #Transformation in an appropriate data structure if needed
    for i in range(height):
        for j in range(width):
            #print(pixels_imgNew[i*width + j])
            color[i][j] = pixels_img_km[i*(width-1) + j]
            pixels_img_grey[i*(width-1) + j] = img_grey[i][j]
            
    return image, img_grey, color, pixels_img_grey, height, width, pixels_imgNew, cluster_centers
   
"""        
###### INTERESTING FUNCTIONS ########
#Discrete Fourier Transformation magnitud        
f = np.fft.fft2(img_grey) #Discrete Fourier Transform
fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
magnitude_spectrum = 20*np.log(np.abs(fshift))

#SIFT descriptors
#surf = cv2.xfeatures2d.SURF_create(400) # Create SURF object. Hessian Threshold to 400
#kp, des = surf.detectAndCompute(img_grey, None) # Find keypoints and descriptors directly
sift = cv2.SIFT_create() # Create SIFT object
kp, descriptors = sift.detectAndCompute(img_grey, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly

# localized mean and standard di=eviation of the intensity
mean = np.average(img_grey)
deviation = np.std(img_grey)
#print(mean, deviation)

# grayscale histogram
grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256]) 
"""
        
class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, K):
        '''
        INPUT :
        - K : is a natural number bigger than 0
        '''
        # empty initialization of X and y
        self.X = []
        self.y = []
        # K is the parameter of the algorithm representing the number of neighborhoods
        self.k = K

    """def train(self,X,y):
        '''
        INPUT :
        - X : is a 2D Nx4 numpy array containing the features
        - y : is a 1D Nxk or N numpy array containing the colors
        '''
        self.X=X # copy your training points
        self.y=y"""

    def predict(self,X_new):
        '''
        INPUT :
        - X_new : is a Mx4 numpy array containing the features of new pixels whose label has to be predicted

        OUTPUT :
        - y_hat : is a Mx1 numpy array containing the predicted labels for the X_new points
        '''
        y_hat = []
        for x in X_new:
            distance = []
            voisin = []
            for p in self.X :
                distance.append((x - p)**2)
            for i in range(self.k):
                v = np.argmin(distance)
                distance.pop(v)
                voisin.append(self.y[v])
            y_hat.append((1/len(voisin)*sum(voisin)))

        return y_hat
        
    def predict_bis(self,X_new, height, width):
        '''
        INPUT :
        - X_new : is a numpy array containing the feature of the new pixel whose label has to be predicted

        OUTPUT :
        - y_hat : is the predicted labels for the X_new points
        '''
        dst = []
        voisin = []
        coordinates = []
        for i in range(height) :
            for j in range(width) :
                dst.append(distance.euclidean(self.X[i,j], X_new))
                coordinates.append([i,j])
        i=0
        while i<self.k and dst!=[]:
            i+=1
            v = np.argmin(dst)
            dst.pop(v)
            voisin.append(self.y[coordinates[v][0], coordinates[v][1]])                  
        
        y_hat = (1/len(voisin)*sum(voisin))                  
        
        return y_hat


def prepare_feature(img_grey, height, width):
    delta=11
    
    x_magnitudeSpectrum = np.zeros((height, width, 121))
    x_grayHist = np.zeros((height, width, 256))
    x_mean = np.zeros((height, width))
    x_deviation = np.zeros((height, width))
    x_descriptors = np.zeros((height, width))
    radius=delta//2
    for i in range(height) :
        for j in range(width) :  
            if (radius<=i<=height-radius-1) and (radius<=j<=width-radius-1) : #change height-radius by height-radius-1 and width-radius by width-radius-1        p = 0
                window=img_grey[i-radius: i+radius+1, j-radius: j+radius+1] #change i-radius+1: i+radius by i-radius: i+radius+1
                #print("a", len(window), len(window[0]), i-radius, i+radius+1, j-radius, j+radius+1, i, j)
                p = 0
            else : 
                a = i-radius
                b = i+radius+1
                c = j-radius
                d = j+radius+1
                if (i < radius) :
                    a = 0
                    b = delta 
                    p = 1
                elif (i > height - 1 - radius) :
                    a = height - 1 -delta
                    b = height - 1
                    p = 2
                if (j < radius):
                    c = 0
                    d = delta
                    p = 3
                elif (j > width - 1 - radius):
                    c = width - 1 -delta
                    d = width - 1
                    p = 4
                window = img_grey[a:b, c:d]
            
            f = np.fft.fft2(window) #Discrete Fourier Transform
            fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
            magnitude_spectrum = 20*np.log(np.abs(fshift))
            #print(len(magnitude_spectrum), len(magnitude_spectrum[0]))
            try:
                x_magnitudeSpectrum[i,j]=np.array(magnitude_spectrum).ravel()
            except ValueError:
                print(i,j,p,a,b, c, d)
            
            grayscale_hist = cv2.calcHist([img_grey], [0], None, [256], [0, 256])
            x_grayHist[i,j]=np.array(grayscale_hist).ravel()
        
            mean = np.average(window)
            deviation = np.std(window)        
            x_mean[i,j]=mean
            x_deviation[i,j]=deviation
            
            """sift = cv2.SIFT_create() # Create SIFT object
            kp, descriptors = sift.detectAndCompute(window, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly
            x_descriptors[i,j] =  descriptors""" #Problem of size
    return x_magnitudeSpectrum, x_grayHist, x_mean, x_deviation
    
def prepare_feature_pixel(img_grey, i, j, height, width):
    delta=11
    radius=delta//2
    
    if (radius<=i<=height-radius-1) and (radius<=j<=width-radius-1) : #change height-radius by height-radius-1 and width-radius by width-radius-1
        window=img_grey[i-radius: i+radius+1, j-radius: j+radius+1] #change i-radius+1: i+radius by i-radius: i+radius+1
           
    else :
        a = i-radius
        b = i+radius+1
        c = j-radius
        d = j+radius+1
        if (i < radius) :
            a = 0
            b = delta
        elif (i > height - 1 - radius) :
            a = height - 1 -delta
            b = height - 1
        if (j < radius):
            c = 0
            d = delta
        elif (j > width - 1 -radius):
            c = width - 1 -delta
            d = width - 1
        window = img_grey[a:b, c:d] 
 
    f = np.fft.fft2(window) #Discrete Fourier Transform
    fshift = np.fft.fftshift(f) #Bring zero frequency component (DC component) from  top left corner to center
    magnitude_spectrum = 20*np.log(np.abs(fshift))
    #print(len(magnitude_spectrum), len(magnitude_spectrum[0]))
    x_magnitudeSpectrum=np.array(magnitude_spectrum).ravel()
            
    grayscale_hist = cv2.calcHist([window], [0], None, [256], [0, 256])  # window or img_grey
    x_grayHist=np.array(grayscale_hist).ravel()

    mean = np.average(window)
    deviation = np.std(window)        
    x_mean=mean
    x_deviation=deviation
            
    """sift = cv2.SIFT_create() # Create SIFT object
    kp, descriptors = sift.detectAndCompute(window, None, useProvidedKeypoints = False) # Find keypoints and descriptors directly
    x_descriptors =  descriptors""" #Problem of size
    
    return x_magnitudeSpectrum, x_grayHist, x_mean, x_deviation

def knn_tests(img_grey, i, j, height, width, color): 
    #x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation = prepare_feature_pixel(img_grey, i, j, height, width)
    #print(x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation)
    knn = KNN(K=5)
    knn.X = img_grey
    knn.y = color
    rep_grey = knn.predict_bis(img_grey[i,j], height, width)
    
    
    return np.array(rep_grey)
    
    
def knn_tests_results(img_unknown, i, j, height, width, img_grey, color):  #used for colorization
    #x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation = prepare_feature_pixel(img_unknown, i, j, height, width)
    #print(x_NewmagnitudeSpectrum, x_NewgrayHist, x_Newmean, x_Newdeviation)
    knn = KNN(K=5)
    knn.X = img_grey
    knn.y = color
    rep_grey = knn.predict_bis(img_unknown[i,j], height, width)
    
   
   
    return np.array(rep_grey)
  
"""
knn = KNN(K=5)
knn.X = pixels_img_grey[0:200]
knn.y = pixels_imgNew[0:200]
rep = knn.predict(pixels_img_grey[0:200])
print(rep, pixels_imgNew[0:200])
"""

### Linear Regression over the answers given by knn for each feature
# We do not know exactly if each feature is really important, so we use a linear regression to attibute to each feature its proper weight

# class linear_regression:
#     def __init__(self) : # initialize constructor for the object to assign the object its properties
#         self.X_train = [] #Contains the features of each pixel
#         self.y_train = [] #Contains the color of each pixel
#         self.weights = []
        
#     def fit(self, X, y) :
#         self.X_train = X
#         self.y_train = y
#         #print(X, y)
#         self.weights = np.linalg.solve(X.T@X,X.T@y)
    
#         """print("X=",X)
#         print("y=",y)
#         print("X1=",np.dot(X.T,X))
#         print("X2=",np.dot(X.T,y))
#         #self.weights = np.linalg.solve(X.T@X,X.T@y)"""
        
#     def test(self,x_test,y_test) : # Compute the MSE
#         self.y_hat=np.sum(x_test*self.weights,axis=1)
        
#         self.MSE= (1/y_test.shape[0])*np.sum(np.square(y_test - self.y_hat))
        
#         return self.y_hat, self.MSE
        
#     def predict(self,x_toPredict) : # predict the color of a pixel
#         self.y_hat=np.sum(x_toPredict*self.weights)
        
#         return self.y_hat



def prepare_data_bis(m, n, color):
    input_train =  np.zeros((m, n)) 
    """TO CHANGE"""
    #input_train =  np.zeros((m, n, 5)) 
    for i in range(m):
        for j in range(n):
            input_train[i,j] = knn_tests()
    return input_train, color[:m, :n]
    
def prepare_data(img_grey, height, width, color):
    #print(img_grey)
    m, n = img_grey.shape
    #NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
    input_train =  np.zeros((m, n)) #3 rajouté car self.y a trois colonnes

    for i in range(m):
        for j in range(n):
            #print(knn_tests(img_grey, NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation, i, j, height, width, color), "ici")
            input_train[i,j] = knn_tests(img_grey,i,j,height,width,color)
    return input_train, color


def prepare_unknown_data_bis(m, n):
    input =  np.zeros((m, n)) 
    #NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img_grey, m, n)
    for i in range(m):
        for j in range(n):
            input[i,j] = knn_tests()
    return input
    
def prepare_unknown_data(img, height, width, img_grey, color):
    m, n = img.shape
    #NewmagnitudeSpectrum, NewgrayHist, Newmean, Newdeviation = prepare_feature(img, m, n)
    input =  np.zeros((m, n)) 
    for i in range(m):
        for j in range(n):
            input[i,j]= knn_tests_results(img, i, j, height, width, img_grey, color)
            
            
    
    return input
            


    

def colorised_image(img_toColorize,class_to_pixels, img_grey, color):
    
    
    height_toColorize, width_toColorize = img_toColorize.shape
    color_img = np.zeros((height_toColorize, width_toColorize, 3))
    input = prepare_unknown_data(img_toColorize, height_toColorize, width_toColorize, img_grey, color)
    #input = np.zeros((height_toColorize, width_toColorize, 5))
    for i in range(height_toColorize) :
        for j in range (width_toColorize) :
            pixel = class_to_pixels[math.floor(input[i,j])]
            for l in range(3):
                    pixel[l] = math.floor(pixel[l])
                    color_img[i][j] = pixel
    
   
    
    
    return color_img.astype(dtype =np.uint8)
    
def colorised_image_bis(img_toColorize, model_linear_regression): #returns the colour class
    height_toColorize, width_toColorize = img_toColorize.shape
    #color_img = np.zeros((height_toColorize, width_toColorize, 3))
    input = prepare_unknown_data(img_toColorize, height_toColorize, width_toColorize)  
    
    return input
    

#print(model_linear_regression.y_hat)
#print(model_linear_regression.MSE)    
def test_prog_single_feature():
    
    image, img_grey, color, pixels_img_grey, height, width, pixels_imgNew, class_to_pixels = preparation()
    resize_img = cv2.resize(image  , (200 , 150))
    #cv2.imshow('img' , resize_img)
     #cv2.waitKey()
     #cv2.imshow('img1',image)
    # cv2.imshow('img',image.astype(dtype =np.float))
    
    input_train, output_train = prepare_data(img_grey, height, width, color)
    print("done")
    input_test, output_test = input_train, output_train
     #input_test, output_test = prepare_data(img_grey, height, width, color)  
     #input = [rep_grey, rep_magnitudeSpectrum, rep_grayHist, rep_mean, rep_deviation]
     
     #input = prepare_unknown_data(m, n)
     
     #There could be some problem of data representation
     #so you need to transform them in an appropriate data structure if needed
    input_train_bis = np.zeros((height * width, 5))
    output_train_bis = np.zeros((height * width))
    input_test_bis = np.zeros((height * width, 5))
    output_test_bis = np.zeros((height * width))
    
    for i in range(height):
        for j in range(width):
         
            input_train_bis[i*width + j] = input_train[i][j]
            output_train_bis[i*width + j] = output_train[i][j]
            input_test_bis[i*width + j] = input_test[i][j]
            output_test_bis[i*width + j] = output_test[i][j]
     
    #model_linear_regression = linear_regression()
    #model_linear_regression.fit(input_train_bis, output_train_bis)
    #model_linear_regression.test(input_test_bis, output_test_bis)
    colorised_img = colorised_image(img_grey,class_to_pixels,img_grey, color)
     #print(colorised_img)
    colorised_img_resize = cv2.resize(colorised_img  , (200 , 150))
    #cv2.imshow('colorized',colorised_img_resize)
    #cv2.waitKey()
    return(resize_img, colorised_img_resize, color)
    
og, colo, classes = test_prog_single_feature() 

def img_accuracy(img_colour, img_predicted, criterion = 100):
    height_img, width_img = img_colour.shape[:2]
    error = 0

    for i in range(height_img) :
        for j in range(width_img) :
            dist = distance.euclidean(img_colour[i][j], img_predicted[i][j])
            if (dist > criterion) :
                error += 1

    #We return a measure of the accuracy, while the error is between 0 and 1
    return (1- error /(height_img * width_img))


def accuracy():
    img_accuracy()


def visual_discrepancy(img_colour, img_predicted, criterion = 0.8, nb_classes = 16):
    '''
    Provides a rough accuracy measure by comparing how "smooth" the predicted image is
    compared to the original one, that is how many perturbations there are in global
    regions composing the image

    Parameters
    ----------
    img_colour : 2D-array
        The original image as a matrix.
    img_predicted : 2D-array
        The predicted image as a matrix.
    criterion : int
        The required smoothness for identifying regions in an image. The default is 0.8
    nb_classes : int, optional
        Number of color classes used. The default is 16.

    Returns
    -------
    int
        A number between 0 (if the smoothnesses are the same) and 1 (if they
        are not similar at all.)

    '''
    height_img, width_img = img_colour.shape[:2] #3-D because there are colors

    smoothness, psmoothness = 0, 0

    regions = split(img_colour, criterion, nb_classes)
    pregions = split(img_predicted, criterion, nb_classes)
    
    #Computing smoothnesses
    for reg in regions:
        smoothness += smooth(reg, nb_classes)
    for preg in pregions:
        psmoothness += smooth(preg, nb_classes)
    diff_segmentation = (len(regions) - len(pregions)) / (height_img * width_img -1) #score up to 1 measuring discrepancy between pictures separation into regions
    return abs(psmoothness-smoothness) * diff_segmentation / (height_img * width_img) #up to 1 


def split(list_regions, criterion = 0.8, nb_classes = 16):
    '''
        Recursive function which divides regions into 4 regions of about the same size while
        they do not meet a uniformity criterion.

    Parameters
    ----------
    list_regions : numpy array of matrices
        A list containing the regions that might have to be splitted.
    criterion : float
        The required smoothness expected from a region in order not to be splitted (see smooth function).

    Returns
    -------
    numpy array
        The numpy array containing the splitted regions.

    '''
    result = list_regions
 
    for reg in list_regions :
        height, width = reg.shape
        
        #Worst case safeguard (single pixel regions)
        if ((height == 1) and (width == 1)):
            continue
        
        smoothness = smooth(reg)
        
        #If the region is not smooth enough, we split it, else we continue with the next 
        #one in result
        if(smoothness < criterion):
            vmid, hmid = height//2, width//2
            r1 = reg[0:vmid][0:hmid]
            r2 = reg[0:vmid][hmid:width]
            r3 = reg[vmid:height][0:hmid]
            r4 = reg[vmid:height][hmid:width]
            
            separations = np.array([r4,r3,r2,r1])
            
            #Replacing first region of array with the four sub-regions
            result = result.remove(reg)
            result = result.reverse()
            result = result.extend(separations)
            result = result.reverse()
            
            #Applying split to new regions
            result = split(result)
           
    return result

def smooth(region, nb_classes = 16):
    
    height, width = region.shape
    classes = np.zeros(nb_classes)
    for element in region:
        classes[element] += 1
    majority = np.max(classes)
    return majority / (height * width) #proportion up to 1 representing the "visual homogeneity"
    
imgacc_crit = 100
imgacc = img_accuracy(colo,og, imgacc_crit)
print('img_accuracy result with criterion equal to', imgacc_crit, ': ', imgacc)

